# Scripts and data attached to CRC study Purcell 2017

**Distinct gut microbiome patterns associate with consensus molecular subtypes of colorectal cancer.**
Purcell RV, Visnovska M, Biggs PJ,  Schmeier S, Frizelle FA.
[Scientific Reports, 2017, doi: 10.1038/s41598-017-11237-6](http://dx.doi.org/10.1038/s41598-017-11237-6)

Pubmed: [https://www.ncbi.nlm.nih.gov/pubmed/28912574](https://www.ncbi.nlm.nih.gov/pubmed/28912574)
