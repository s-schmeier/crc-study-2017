# qPCR data plotting of fold-changes for CRC sub-types

```bash
# Install miniconda
curl -O https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
bash Miniconda2-latest-Linux-x86_64.sh

# Add conda dir to PATH
echo 'export PATH="~/miniconda2/bin:$PATH"' >> ~/.bashrc

# Install bioconda
conda config --add channels conda-forge
conda config --add channels defaults
conda config --add channels r
conda config --add channels bioconda

# make env
conda create --name crc --file conda-packages.txt
source activate crc

# Run workflow
make run
```