#!/usr/bin/env python
"""
NAME: {{name}}
=========

DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

VERSION HISTORY
===============

{{version}}   {{date}}    Initial version.

LICENCE
=======
@2016, copyright {{author}} ({{email}}), {{url}}@

template version: 1.6 (2016/11/09)
"""
from timeit import default_timer as timer
from signal import signal, SIGPIPE, SIG_DFL
import sys
import os
import os.path
import argparse
import csv
import collections
import gzip
import bz2
import zipfile
import time

try:
    from colorama import init, Fore, Style
except ImportError:
    sys.stderr.write('colorama lib needed.\n')
    sys.exit(1)

# When piping stdout into head python raises an exception
# Ignore SIG_PIPE and don't throw exceptions on it...
# (http://docs.python.org/library/signal.html)
signal(SIGPIPE, SIG_DFL)


# INIT color
# Initialise colours for multi-platform support.
init()

# Initialise input for multi-version support.
try:
    input = raw_input
except NameError:
    pass

def red(text):
    return '%s%s%s' % (Fore.RED, text, Fore.RESET)
def yellow(text):
    return '%s%s%s' % (Fore.YELLOW, text, Fore.RESET)
def cyan(text):
    return '%s%s%s' % (Fore.CYAN, text, Fore.RESET)
def err(text, exit=False):
    sys.stderr.write(red('%s\n'%text))
    if exit: sys.exit(1)

__version__ = '{{version}}'
__date__ = '{{date}}'
__email__ = '{{email}}'
__author__ = '{{author}}'


def parse_cmdline():
    """ Parse command-line args. """
    ## parse cmd-line -----------------------------------------------------------
    description = 'Read delimited file.'
    version = 'version %s, date %s' % (__version__, __date__)
    epilog = 'Copyright %s (%s)' % (__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument('--version',
                        action='version',
                        version='%s' % (version))

    parser.add_argument(
        'str_file',
        metavar='FILE',
        help=
        'Delimited file. [if set to "-" or "stdin" reads from standard in]')
    parser.add_argument(
        'ref_file',
        metavar='FILE',
        help=
        'REF')
   
    parser.add_argument('-o',
                        '--out',
                        metavar='STRING',
                        dest='outfile_name',
                        default=None,
                        help='Out-file. [default: "stdout"]')

    

    # if no arguments supplied print help
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    
    args = parser.parse_args()
    return args, parser


def load_file(filename):
    """ LOADING FILES """
    if filename in ['-', 'stdin']:
        filehandle = sys.stdin
    elif filename.split('.')[-1] == 'gz':
        filehandle = gzip.open(filename)
    elif filename.split('.')[-1] == 'bz2':
        filehandle = bz2.BZFile(filename)
    elif filename.split('.')[-1] == 'zip':
        filehandle = zipfile.Zipfile(filename)
    else:
        filehandle = open(filename)
    return filehandle



def main():
    """ The main funtion. """
    args, parser = parse_cmdline()

    fileobj_ref = load_file(args.ref_file)
    fileobj = load_file(args.str_file)

    # create outfile object
    if not args.outfile_name:
        outfileobj = sys.stdout
    elif args.outfile_name in ['-', 'stdout']:
        outfileobj = sys.stdout
    elif args.outfile_name.split('.')[-1] == 'gz':
        outfileobj = gzip.open(args.outfile_name, 'wb')
    else:
        outfileobj = open(args.outfile_name, 'w')

    # ------------------------------------------------------

    d = {}
    reader = csv.reader(fileobj_ref, delimiter = '\t')
    header = reader.next()
    sys.stderr.write('%s\n'%'\t'.join(header))
    for a in reader:
        d[a[0]] = a

    # PARSE QIIME OTU TABLE
    reader = csv.reader(fileobj, delimiter = '\t')
   

    import re
    reg = re.compile('p\_\_(.+)')
    otu = []
    for a in reader:
        if a[0].startswith('#'):
            a[0] = a[0].replace('#',"")
            outfileobj.write('%s\n'%'\t'.join(a))
        else:
            res = reg.search(a[0])
            #print a[0]
            if res:
                tax = res.group(1)
                if tax in d:
                    a[0] = tax
                    otu.append(a)
    
    otu.sort()

    kraken = []
    for a in otu:
        outfileobj.write('%s\n'%('\t'.join(a)))
        kraken.append(d[a[0]])        

    for a in kraken:
        sys.stderr.write('%s\n'%('\t'.join(a)))
        
    # ------------------------------------------------------
    outfileobj.close()
    return


if __name__ == '__main__':
    sys.exit(main())

