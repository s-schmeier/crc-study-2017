library(corrplot)
## only 16S
otu <- read.table('../../../data/16S/otu_table_sorted_L2.txt.gz', header = F, sep = "\t", skip=2, comment.char='')
header <- read.table("../../../data/16S/otu_table_sorted_L2.txt.gz", nrows = 1, header = FALSE, sep ='\t', stringsAsFactors = FALSE, comment.char = "~", skip=1)

colnames(otu) <- sub("CRC", "16S-", unlist(header))
rownames(otu) <- otu[,1]
otu[,1] <- NULL

c <- cor(otu, method='spearman')
tiff(filename = "phylum-16S-vs-16S-cor.tiff",
     width = 1400, 
     height = 1400, 
     units = "px", 
     pointsize = 24,
     compression = "jpeg",
     bg = "white")
corrplot(c, method="square", type="full", tl.col="black", tl.cex=1.4, cl.cex = 1.4)
dev.off()
dim(otu)

## Kraken only
kraken <- read.table('../../../data/kraken/rnaseq-kraken-reports-combined-phylum-abundances-nocellines.txt.gz', header = TRUE, row.names=1, sep = "\t")
colnames(kraken) <- sub("CRC", "K-", colnames(kraken))
c <- cor(kraken, method='spearman')
tiff(filename = "phylum-K-vs-K-cor.tiff",
     width = 1400, 
     height = 1400, 
     units = "px", 
     pointsize = 24,
     compression = "jpeg",
     bg = "white")
corrplot(c, method="square", type="full", tl.col="black", tl.cex=1.4, cl.cex = 1.4)
dev.off()
dim(kraken)

## subsets
kraken <-  read.table('kraken-phylum-subset.txt.gz', header = TRUE, row.names=1, sep = "\t")
otu <- read.table('otu_table_sorted_L2-subset.txt.gz', header = TRUE, row.names=1, sep = "\t", skip=1)
kraken <- kraken[,names(kraken) %in% colnames(otu)]
otu <- otu[,names(otu) %in% colnames(kraken)]
colnames(kraken) <- sub("CRC", "K-", colnames(kraken))
colnames(otu) <- sub("CRC", "16S-", colnames(otu))
c <- cor(kraken, otu , method='spearman')

tiff(filename = "phylum-K-vs-16S-cor.tiff",
     width = 1400, 
     height = 1400, 
     units = "px", 
     pointsize = 24,
     compression = "jpeg",
     bg = "white")
corrplot(c, 
         method="square", 
         type="full", 
         tl.col="black", 
         tl.cex=1.4, 
         cl.cex = 1.4
         )
dev.off()

crc <- diag(c)
mean(crc)


#tiff(filename = "phylum-K-vs-16S-cor-barplot.tiff",
#     width = 1400, 
#     height = 800, 
#     units = "px", 
#     pointsize = 13,
#     compression = "jpeg",
#     bg = "white")

setEPS()
postscript("phylum-K-vs-16S-cor-barplot.eps", height = 8, width = 18)
barplot(crc, 
        names.arg=sub("K-", "", colnames(kraken)),
        ylim = c(0,1),  
        col="#495b5b", 
        xlab="CRC samples", 
        ylab="Correlation",
        border = NA, 
        cex.axis=1.4, 
        cex.names=1.4, 
        cex.lab=1.4,
        lwd = 3,
        legend.text="Kraken vs 16S (phylum-level)",
        args.legend = list(x = "topleft", cex=1.4,  bty = "n")
        )
abline(h=mean(crc),col='#000000',lty=2, lwd=3)
dev.off()
