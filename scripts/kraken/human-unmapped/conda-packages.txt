# This file may be used to create an environment using:
# $ conda create --name <env> --file <this file>
# platform: linux-64
ea-utils=1.1.2.537=0
fastqc=0.11.5=1
flash=1.2.11=0
java-jdk=8.0.92=1
jellyfish=1.1.11=1
kraken=0.10.6_eaf8fb68=2
kraken-all=0.10.6_eaf8fb68=0
krona=2.7=0
libgcc=5.2.0=0
mkl=2017.0.1=0
numpy=1.11.3=py36_0
openssl=1.0.2j=0
pandas=0.19.2=np111py36_1
perl-threaded=5.22.0=10
pip=9.0.1=py36_1
python=3.6.0=0
python-dateutil=2.6.0=py36_0
pytz=2016.10=py36_0
readline=6.2=2
setuptools=27.2.0=py36_0
six=1.10.0=py36_0
sqlite=3.13.0=0
tk=8.5.18=0
wheel=0.29.0=py36_0
xz=5.2.2=1
zlib=1.2.8=3
