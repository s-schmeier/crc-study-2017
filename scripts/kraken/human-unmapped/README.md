# CRC Kraken run of unmapped (human) rnaseq reads

## Workflow

```bash
# Requirements
# Create conda env if not exists, rq. bioconda
conda env create -n py3-kraken -f conda-packages.txt

# we run kraken for each file
git clone git@github.com:sschmeier/kraken-analysis.git
cp kraken-analysis/*.py .
cp kraken-analysis/*.sh .
bash run-kraken-analysis.sh py3-kraken rnaseq data kraken-db-b-201701-crc ~/anaconda2/envs/py3-kraken/opt/krona/taxonomy

# kraken summary reports
for fn in rnaseq-kraken/*.kraken; do 
    echo $fn; 
    kraken-mpa-report --db kraken-db-b-201701-crc $fn | gzip > $fn.mpareport.gz; 
done
```

Creation of a custom Kraken database
------------------------------------

All NCBI refseq bacterial genomes with "Complete Genomes"- or "Chromosome"-level genomes were downloaded from NCBI FTP site (ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/) based on information in the "assembly_summary.txt" file as of 19th January 2017. 
A list of the genomes can be accessed in Supplementary Table K1. 
Additional genomes known to play role in CRC were added disregarding their genome status (see Supplementary Table K2). 
Using the genome fasta-files, a new Kraken database was created using "kraken-build --build" with default parameter. 
The resulting database had a size of 131GB.


Taxonomic classification of RNA-seq reads
-----------------------------------------

All RNA-seq reads that we were unable to map to the human reference genome were extracted per sample and were input to Kraken using our custom Kraken database for taxonomic classification (see above). 
This resulted in bacterial abundances per CRC sample based on unmapped RNA-seq reads.




