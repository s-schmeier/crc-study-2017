args = commandArgs(TRUE)

#
# usage:
# R --vanilla --slave --args \
#	inFile outFile \
#	< classify_samples_with_CMSclassifier.R 
#	> classify_samples_with_CMSclassifier.measure.log 2>&1
#

# use CMSclassifier to classify the input file and 
# write the output table into the output file

#install.packages("devtools", repos="https://cran.stat.auckland.ac.nz/")
library(devtools)
#install_github("Sage-Bionetworks/CMSclassifier", force=TRUE)
library(CMSclassifier)
sessionInfo()

inFile <- as.character(args[1])
outFile <- as.character(args[2])

samples_x_genes_table <- read.table(
	inFile,
	header = TRUE,
	row.names = 1, 
	check.names = FALSE	
)

classified_samples <- CMSclassifier::classifyCMS(
	t(log2(samples_x_genes_table+1)), 
	method="SSP"
)[[3]]

write.table(
	classified_samples,
        file=outFile,
        row.names=TRUE,
        col.names=TRUE,
        sep="\t",
        quote=FALSE
)
