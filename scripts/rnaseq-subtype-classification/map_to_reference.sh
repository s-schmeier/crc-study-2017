#!/bin/bash

#
# script to map reads to reference by a specified tool (bowtie2 and bwa mem).
#

usage(){
        echo "Usage:"
	echo "bash $0 \ "
	echo "-r <fasta_reference> \ "
	echo "-i <index_folder> \ "
	echo "-f <analysis_folder> (solexaQA lengthsort output folder at depth 2) \ "
	echo "-p <patternForBaseNameList> \ "
	echo "-t <tool> (bowtie2 or bwamem) \ "
	echo "-o <additional_options_to_pass_to_the_tool> \ "
	echo "-O <additional_options_no2_to_pass_to_bwa_sampe> \ "
	echo "-R <addReadGroup> [YES/NO] \ "
	echo "-T <testRun> [YES/NO] "
	}


while getopts ":r:i:f:p:t:o:O:R:T:h" OPT; do
	case $OPT in
		r) FASTA_REFERENCE=$OPTARG
		;;
		i) INDEXfolder=$OPTARG
		;;
		f) ANALYSISfolder=$OPTARG
		;;
		p) PATTERN=$OPTARG
		;;
		t) TOOL=$OPTARG
		;;
		o) ADDITIONALoptions=$OPTARG
		;;
		O) ADDITIONALoptions2=$OPTARG
		;;
		R) addReadGroup=$OPTARG
		;;
		T) testRun=$OPTARG
		;;
		h) usage
		exit 0
		;;
		\?) #unrecognized option - show usage
		echo -e "\nOption -$OPTARG not allowed.\n"
		usage
		exit 2
		;;
	esac
done

source ./utils.sh

echo "#!/bin/bash"
echo "# Script $0 starts: "$(date)
echo
echo "# FASTA_REFERENCE=${FASTA_REFERENCE}"
echo "# INDEXfolder=${INDEXfolder}"
echo "# ANALYSISfolder=${ANALYSISfolder}" # output of SolexaQA++ lengthsort
echo "# PATTERN=${PATTERN}"
echo "# TOOL=${TOOL}"
echo "# ADDITIONALoptions=${ADDITIONALoptions}"
echo "# ADDITIONALoptions2=${ADDITIONALoptions2}"
echo "# addReadGroup=${addReadGroup}"
echo "# testRun=${testRun}"

basenameList=$(pattern_to_file_basename_list "${PATTERN}")

PATTERNfolder=${PATTERN%/*}

echo "# PATTERNfolder=${PATTERNfolder}"
echo
echo "# basenameList=${basenameList}"
echo

flowcellID=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 1 -d ' ' | sort | uniq)
projectID=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 2 -d ' ' | sort | uniq)
sampleIDs=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 3 -d ' ' | sort | uniq)
libraryTypeCode=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 4 -d ' ' | sort | uniq)
libraryVersion=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 5 -d ' ' | sort | uniq)
sampleSheetIDs=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 6 -d ' ' | sort | uniq)
laneIDs=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 7 -d ' ' | sort | uniq)
suffix=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_\|\./ /g' | cut -f 9 -d ' ' | sort | uniq)

fasta_name=$(basename ${FASTA_REFERENCE})
fasta_folder=${FASTA_REFERENCE%/*}

samples=""
lanes=""
if [ "${testRun}" = "YES" ]; then
	samples="01 02 03"
	lanes="L002 L003"
else
	samples=${sampleIDs[@]}
	lanes=${laneIDs[@]}
fi


for sample in ${samples}; do
        for lane in ${lanes}; do

		# fileNamePrefix: prefix up to readIDs (R1/R2) of the files that going to be processed (readID not included)
		# fileNameSuffix: suffix from readID to the end of the fileNames (readID not included)
		# standard file names passed to the script clean_one_dataset.sh have the following form:
		# R1(forward reads): ${flowcellID}-${projectID}-${sample}-${libraryTypeCode}-${libraryVersion}_${sampleSheetID}_${lane}_R1_${suffix}.fastq.gz
		# R2(reverse reads): ${flowcellID}-${projectID}-${sample}-${libraryTypeCode}-${libraryVersion}_${sampleSheetID}_${lane}_R2_${suffix}.fastq.gz

		# warning: further code in clean_one_dataset.sh assumes the last character in fileNamePrefix to be '_' and trim it when appropriate.

		list=$(pattern_to_file_basename_list "${PATTERNfolder}/${flowcellID}-${projectID}-${sample}-${libraryTypeCode}-${libraryVersion}_*_${suffix}.fastq.gz")

		sampleSheetID=$(echo ${list} | sed -r 's/\s+/\n/g' |  sed -e 's/-\|_/ /g' | cut -f 6 -d ' ' | sort | uniq)
		fileNamePrefix="${flowcellID}-${projectID}-${sample}-${libraryTypeCode}-${libraryVersion}_${sampleSheetID}_${lane}_"
		fileNameSuffix='.noAdapt_${suffix}.fastq.trimmed.paired.gz'


		readGroup=""
		if [ "${addReadGroup}" = "YES" ]; then
			ID='D00540' # from read headers in fastq files, the string between @ and the first colomn
			PL='ILLUMINA'
			readGroup=ID:${ID}.${lane}$'\t'PU:${flowcellID}.${lane}$'\t'SM:${sample}$'\t'LB:${sample}$'\t'PL:${PL}
		else
			readGroup=""
		fi

		echo "${readGroup}"
		echo

		#check availability of cluster nodes (bhosts status ok, no more than one star mapping job per cluster running)
		free_nodes_str=""

		while [ "${free_nodes_str}" = "" ]; do

	                free_nodes=( 0 0 0 0 0 0 0 )

        	       	for i in 1 2 3 4 5 6; do
                	        NJOBS=$( bjobs -w | grep "com00${i}.genomics.local" | grep "map_one_dataset" | wc -l )
                        	free_nodes[${i}-1]=${NJOBS}
	                done

        	        for i in 1 2 3 4 5 6; do
                	        STATUS=$( bhosts -w com00${i}.genomics.local | grep "com00${i}.genomics.local" | awk '{print $2}' )
                        	if [ "${STATUS}" != "ok" ]; then
                                	free_nodes[${i}-1]=-1
	                        fi
        	        done

			for i in 1 2 3 4 5 6; do
				if [ "${free_nodes[${i}-1]}" = "0" ]; then
					free_nodes_str=${free_nodes_str}" "${i}
				fi
			done

			free_nodes_str=$( echo ${free_nodes_str} | tr ' ' ',')

			echo
			echo "free_nodes_str=${free_nodes_str}"
			echo

			process_command "sleep 60" 0 -
		done

		echo "# Mapping: "$(date)
		process_command "bsub -cwd $(pwd) \
			-o map_one_dataset.%J.stdout \
			-e map_one_dataset.%J.stderr \
			-m com00[${free_nodes_str}].genomics.local \
			./map_one_dataset.sh \
				-f ${fasta_name} \
				-F ${fasta_folder} \
				-I ${INDEXfolder} \
				-q ${ANALYSISfolder}/${flowcellID}-${projectID}-${sample}/${fileNamePrefix%_}/lengthsort \
				-P ${fileNamePrefix} \
				-S ${fileNameSuffix} \
				-t ${TOOL} \
				-o \"${ADDITIONALoptions}\" \
				-O \"${ADDITIONALoptions2}\" \
				-r \"${readGroup} \" \
				" 0 -

		process_command "sleep 10" 0 -
	done
done


#if [ "${TOOL}" = "STAR" ]; then
#        echo
#        echo "# Remove genome index from memory after mapping: "$(date)
#
#        process_command "STAR \
#                --outFileNamePrefix ${ANALYSISfolder}/removeIndex_STAR_after_ \
#                --genomeLoad Remove \
#                --genomeDir ${INDEXfolder} \
#               	> $(pwd)/removeIndex_STAR_after.stdout \
#                2> $(pwd)/removeIndex_STAR_after.stderr" 0 -
#fi

echo
echo "# Script $0 ends: "$(date)
