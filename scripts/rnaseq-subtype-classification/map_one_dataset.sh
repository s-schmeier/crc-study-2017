#!/bin/bash

#
# script to map cleaned data for one lane of one sample (HiSeq)
#

usage(){
	echo "Usage:"
	echo "bash $0 \ "
	echo "-f <referenceFastaName> -q <folderWithMappingFastqFiles> \ "
	echo "-F <fastaFolder> -I <indexFolder> \ "
	echo "-P <fastqFilePrefixToReadID-R1/R2> -S <fastqFileSuffixAfterReadID-R1/R2> \ "
	echo "-t <tool-bowtie2/bwamem/bwa/STAR> \ "
	echo "-o <mapperOptions1> -O <mapperOptions2-passed_to_bwa_sampe> \ "
	echo "-r <readGroupToAdd> "
}

while getopts ":f:F:I:q:P:S:t:o:O:r:h" OPT; do
	case $OPT in
		f) fasta_name=$OPTARG
		;;
		F) FASTAfolder=$OPTARG
		;;
		I) INDEXfolder=$OPTARG
		;;
		q) FASTQfolder=$OPTARG
		;;
		P) fileNamePrefix=$OPTARG
		;;
		S) fileNameSuffix=$OPTARG
		;;
		t) TOOL=$OPTARG
		;;
		o) ADDITIONALoptions=$OPTARG
		;;
		O) ADDITIONALoptions2=$OPTARG
		;;
		r) readGroupToAdd=$OPTARG
		;;
		h) usage
		exit 0
		;;
		\?) #unrecognized option - show usage
		echo -e "\nOption -$OPTARG not allowed.\n"
		usage
		exit 2
		;;
	esac
done

source ./utils.sh

fileNameSuf=${fileNameSuffix%.*}
log=${FASTQfolder}/../data_mapping.${TOOL}.${fileNamePrefix%_}.log
fasta_noExt=${fasta_name%.*}
OUTfolder=${FASTQfolder}/../mapping-${TOOL}

echo > ${log}
echo >> ${log}
echo "# Processing sample ${fileNamePrefix%_} by script $0 starts: "$(date) >> ${log}
echo >> ${log}

#
# print parameters to log
#

echo "# fasta_name=${fasta_name}" >> ${log}
echo "# fasta_noExt=${fasta_noExt}" >> ${log}
echo "# FASTAfolder=${FASTAfolder}" >> ${log}
echo "# INDEXfolder=${INDEXfolder}" >> ${log}
echo "# FASTQfolder=${FASTQfolder}" >> ${log}
echo "# fileNamePrefix=${fileNamePrefix}" >> ${log}
echo "# fileNameSuffix=${fileNameSuffix}" >> ${log}
echo "# TOOL=${TOOL}" >> ${log}
echo "# ADDITIONALoptions=${ADDITIONALoptions}" >> ${log}
echo "# ADDITIONALoptions2=${ADDITIONALoptions2}" >> ${log}
readGroupToAdd=${readGroupToAdd:0:${#readGroupToAdd}-1}
echo "# readGroupToAdd=${readGroupToAdd}" >> ${log}
echo "# OUTfolder=${OUTfolder}" >> ${log}
echo "# log=${log}" >> ${log}
echo >> ${log}

#
# create new output folders
#

process_command "mkdir -p ${OUTfolder}" 0 ${log}

#
# create tmp folders
#

# used for:
# 0) temp storage of mapped fastq.gz/fastq files
# 1) storage and postprocess of mapper's output before moving outputs to OUTfolder
TMPfolder=$(mktemp -d /tmp/mapping.XXXXXXXXX)

echo 'TMPfolder=$(mktemp -d /tmp/mapping.XXXXXXXXX)' >> ${log}

#
# cp or gunzip fastq.gz files to /tmp, add fastq extension to unzipped
#
# room for improvement: don't unzip STAR input files, just pass them
# to star with appropriate --readFilesCommand and adjust the following loop accordingly
#
echo >> ${log}
echo "# Unzip fastq files to /tmp: "$(date) >> ${log}
for r in R1 R2; do
	process_command "gunzip -c \
		${FASTQfolder}/${fileNamePrefix}${r}${fileNameSuffix} \
		> ${TMPfolder}/${fileNamePrefix}${r}${fileNameSuf}.fastq" 0 ${log}
	echo >> ${log}
done

#
# run the specified mapper
#
echo >> ${log}
echo "# Run ${TOOL} mapper: "$(date) >> ${log}
if [ "$TOOL" = "bowtie2" ]; then


	echo "# "$(bowtie2 --version) >> ${log}
	echo >> ${log}
	process_command "bowtie2 \
		--reorder -q --phred33 -t \
		${ADDITIONALoptions} \
		-x ${INDEXfolder}/${fasta_noExt}\
		-1 ${TMPfolder}/${fileNamePrefix}R1${fileNameSuf}.fastq \
		-2 ${TMPfolder}/${fileNamePrefix}R2${fileNameSuf}.fastq \
		-S ${TMPfolder}/${fileNamePrefix%_}.sam \
		> ${OUTfolder}/${fileNamePrefix%_}.mapping_bowtie2.stdout \
		2> ${OUTfolder}/${fileNamePrefix%_}.mapping_bowtie2.stderr" 0 ${log}


elif [ "$TOOL" = "bwamem" ]; then


	echo "# "$(bwa) >> ${log}
	echo >> ${log}
	#consider flag -M, if you plan to use picard's markDuplicates
	process_command "bwa mem \
		${ADDITIONALoptions} \
		${FASTAfolder}/${fasta_name} \
		${TMPfolder}/${fileNamePrefix}R1${fileNameSuf}.fastq \
		${TMPfolder}/${fileNamePrefix}R2${fileNameSuf}.fastq \
		> ${TMPfolder}/${fileNamePrefix%_}.sam \
		2> ${OUTfolder}/${fileNamePrefix%_}.mapping_bwamem.stderr" 0 ${log}


elif [ "$TOOL" = "bwa" ]; then


	echo "# "$(bwa) >> ${log}
	echo >> ${log}

	process_command "bwa aln \
		${ADDITIONALoptions} \
		${FASTAfolder}/${fasta_name} \
		${TMPfolder}/${fileNamePrefix}R1${fileNameSuf}.fastq \
		> ${TMPfolder}/${fileNamePrefix}R1${fileNameSuf}.sai \
		2> ${OUTfolder}/${fileNamePrefix%_}.mapping_bwaalnR1.stderr" 0 ${log}

	process_command "bwa aln \
		${ADDITIONALoptions} \
		${FASTAfolder}/${fasta_name} \
		${TMPfolder}/${fileNamePrefix}R2${fileNameSuf}.fastq \
		> ${TMPfolder}/${fileNamePrefix}R2${fileNameSuf}.sai \
		2> ${OUTfolder}/${fileNamePrefix%_}.mapping_bwaalnR2.stderr" 0 ${log}

	process_command "bwa sampe \
		${ADDITIONALoptions2} \
		${FASTAfolder}/${fasta_name} \
		${TMPfolder}/${fileNamePrefix}R1${fileNameSuf}.sai \
		${TMPfolder}/${fileNamePrefix}R2${fileNameSuf}.sai \
		${TMPfolder}/${fileNamePrefix}R1${fileNameSuf}.fastq \
		${TMPfolder}/${fileNamePrefix}R2${fileNameSuf}.fastq \
		> ${TMPfolder}/${fileNamePrefix%_}.sam \
		2> ${OUTfolder}/${fileNamePrefix%_}.mapping_bwasampe.stderr" 0 ${log}


elif [ "$TOOL" = "STAR" ]; then
#do not use STAR for DNAseq if you want to call SNPs,
#the tool doesn't give reasonable MAQ mapping scores


	echo "# "$(STAR --version) >> ${log}
	echo >> ${log}

	process_command "STAR \
		${ADDITIONALoptions} \
		--genomeDir ${INDEXfolder} \
		--readFilesIn ${TMPfolder}/${fileNamePrefix}R1${fileNameSuf}.fastq ${TMPfolder}/${fileNamePrefix}R2${fileNameSuf}.fastq \
		--outFileNamePrefix ${TMPfolder}/${fileNamePrefix%_} \
		> ${OUTfolder}/${fileNamePrefix%_}.mapping_STAR.stdout \
		2> ${OUTfolder}/${fileNamePrefix%_}.mapping_STAR.stderr" 0 ${log}

	echo >> ${log}
	echo "# rename bam files for consistency between the tools" >> ${log}
	process_command "mv -u ${TMPfolder}/${fileNamePrefix%_}*.bam \
		${TMPfolder}/${fileNamePrefix%_}.sortedByCoord.bam" 0 ${log}

	echo >> ${log}
	echo "# list folder ${TMPfolder}" >> ${log}
	process_command "ls -la ${TMPfolder}" 0 ${log}

	echo >> ${log}
	echo "# move STAR log files" >> ${log}
	process_command "mv -u \
		${TMPfolder}/${fileNamePrefix%_}Log*.out ${OUTfolder}" 0 ${log}

       	if [ -f "${TMPfolder}/${fileNamePrefix%_}ReadsPerGene.out.tab" ]; then
               	echo >> ${log}
               	echo "# move ReadsPerGene.out.tab" >> ${log}
               	process_command "mv -u \
                       	${TMPfolder}/${fileNamePrefix%_}ReadsPerGene.out.tab \
                       	${OUTfolder}/${fileNamePrefix%_}ReadsPerGene.out.tab" 0 ${log}
       	fi

	if [ -f "${TMPfolder}/${fileNamePrefix%_}SJ.out.tab" ]; then
		echo >> ${log}
		echo "# move SJ.out.tab" >> ${log}
		process_command "mv -u \
			${TMPfolder}/${fileNamePrefix%_}SJ.out.tab \
			${OUTfolder}/${fileNamePrefix%_}SJ.out.tab" 0 ${log}
	fi

	if [ -f "${TMPfolder}/${fileNamePrefix%_}Unmapped.out.mate1" ]; then
		echo >> ${log}
		echo "# move unmapped.mate1" >> ${log}
		process_command "mv -u \
			${TMPfolder}/${fileNamePrefix%_}Unmapped.out.mate1 \
			${OUTfolder}/${fileNamePrefix%_}Unmapped.out.mate1" 0 ${log}
	fi

        if [ -f "${TMPfolder}/${fileNamePrefix%_}Unmapped.out.mate2" ]; then
		echo >> ${log}
		echo "# move unmapped.mate2" >> ${log}
                process_command "mv -u \
                        ${TMPfolder}/${fileNamePrefix%_}Unmapped.out.mate2 \
                        ${OUTfolder}/${fileNamePrefix%_}Unmapped.out.mate2" 0 ${log}
        fi

else
	echo "Unknown mapping tool: ${TOOL}" >> ${log}
	exit 1
fi


#
# rm the input fastq files
#
echo >> ${log}
echo "# Remove the input fastq files and sam file from /tmp: "$(date) >> ${log}
for r in R1 R2; do
        process_command "rm -f ${TMPfolder}/${fileNamePrefix}${r}${fileNameSuf}.fastq" 0 ${log}
done


#
# postprocess sam/bam mapping output to get sorted and indexed bam file
#
echo >> ${log}
echo "# Post-process mapped sam/bam to sorted and indexed bam file: "$(date) >> ${log}
# using the most recent version nowadays
module load samtools/1.3.1
echo "# which samtools: "$(which samtools) >> ${log}
echo "# samtools: "$(samtools) >> ${log}
echo >> ${log}

if [ "$TOOL" = "bowtie2" ] || [ "$TOOL" = "bwamem" ] || [ "$TOOL" = "bwa" ]; then

	process_command "samtools sort \
		-O bam \
		-o ${TMPfolder}/${fileNamePrefix%_}.sortedByCoord.${TOOL}.bam \
		-@ 4 \
		${TMPfolder}/${fileNamePrefix%_}.sam \
		2> ${OUTfolder}/${fileNamePrefix%_}.samtools_sort.stderr" 0 ${log}

	process_command "rm -f ${TMPfolder}/${fileNamePrefix%_}.sam" 0 ${log}

elif [ "$TOOL" = "STAR" ]; then

	process_command "mv -u ${TMPfolder}/${fileNamePrefix%_}.sortedByCoord.bam \
		${TMPfolder}/${fileNamePrefix%_}.sortedByCoord.${TOOL}.bam" 0 ${log}

else
	module unload samtools
	echo "Unknown mapping tool: ${TOOL}"
	exit 1
fi


if [ "$TOOL" = "bowtie2" ] || [ "$TOOL" = "bwamem" ] || [ "$TOOL" = "bwa" ] || [ "$TOOL" = "STAR" ]; then
	if [ "${readGroupToAdd}" != "" ]; then

		process_command "samtools addreplacerg -r \"${readGroupToAdd}\" -m overwrite_all \
                        -o ${TMPfolder}/${fileNamePrefix%_}.sortedByCoord.${TOOL}.rg.sam \
                       	${TMPfolder}/${fileNamePrefix%_}.sortedByCoord.${TOOL}.bam \
                        2> ${OUTfolder}/${fileNamePrefix%_}.samtools_addreplacerg.stderr" 0 ${log}

		process_command "rm -f ${TMPfolder}/${fileNamePrefix%_}.sortedByCoord.${TOOL}.bam" 0 ${log}

		process_command "samtools view -b -o ${TMPfolder}/${fileNamePrefix%_}.sortedByCoord.${TOOL}.rg.bam \
				${TMPfolder}/${fileNamePrefix%_}.sortedByCoord.${TOOL}.rg.sam" 0 ${log}

		process_command "rm -f ${TMPfolder}/${fileNamePrefix%_}.sortedByCoord.${TOOL}.rg.sam" 0 ${log}

                process_command "samtools index -b \
                        ${TMPfolder}/${fileNamePrefix%_}.sortedByCoord.${TOOL}.rg.bam \
                        2> ${OUTfolder}/${fileNamePrefix%_}.samtools_index.stderr" 0 ${log}

	else

		process_command "samtools index -b \
			${TMPfolder}/${fileNamePrefix%_}.sortedByCoord.${TOOL}.bam \
			2> ${OUTfolder}/${fileNamePrefix%_}.samtools_index.stderr" 0 ${log}

	fi
else
	module unload samtools
	echo "Unknown mapping tool: ${TOOL}"
	exit 1
fi

module unload samtools

if [ "$TOOL" = "bowtie2" ] || [ "$TOOL" = "bwamem" ] || [ "$TOOL" = "bwa" ] || [ "$TOOL" = "STAR" ]; then
	echo >> ${log}
	echo "# Move mapped files from /tmp:"$(date) >> ${log}
	process_command "mv -u ${TMPfolder}/${fileNamePrefix%_}.sortedByCoord.${TOOL}* ${OUTfolder}" 0 ${log}
else
	echo "Unknown mapping tool: ${TOOL}" >> ${log}
	exit 1
fi

echo >> ${log}
echo "# Script $0 ends: "$(date) >> ${log}
