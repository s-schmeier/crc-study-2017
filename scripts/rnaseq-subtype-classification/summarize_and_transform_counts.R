args = commandArgs(TRUE)

###########
#
# the script takes four parameters:
# 1) folder where input data are located,
# 2) filename substring that is present in each of the file
#    names and will be discarded from sample names,
# 3) genome annotation in gtf format, and
# 4) type of output "counts"/"cpm"/"tpm"/"fpkm"
#
# The script convert readcount input tables with ENSEMBL gene IDs
# to one table containing each specified sample in one row and
# gene ENTREZ ID in a column for each identified gene. In a cell x,y
# expression level of gene y in sample x is written in tpm/fpkm/cpm/counts
# depending on the parameter settings.
#
##########

#
# usage:
# R --vanilla --slave --args \
#	directory string annotation tpm/fpkm/counts/cpm \
#	< summarize_and_transform_counts.R \
#	> summarize_and_transform_counts.measure.R 2>&1
#

# example of used parameter setting:
# directory <- "/active/nzgl02041/HiSeq/analysis/read.counts.havana.tTranscript.mNonempty.sNo/"
# fileNameSubstr <- ".uniq.dedup.tTranscript.mNonempty.sNo.havana.counts"
# gtffile <- "/databases/igenomes/Homo_sapiens/NCBI/GRCh38/Annotation/Genes.gencode/genes.gtf"
# out_type <- "tpm"/"fpkm"/"counts"/"cpm"

print("args:")
print(args)

library(DESeq2)
library("GenomicFeatures")
library("org.Hs.eg.db")
library(edgeR)

sessionInfo()

fpkmToTpm <- function(fpkm)
{
    exp(log(fpkm) - log(sum(fpkm)) + log(1e6))
}

directory <- as.character(args[1])
fileNameSubstr <- as.character(args[2])
gtffile <- as.character(args[3])
out_type <- as.character(args[4])

#print(directory)
#print(fileNameSubstr)
#print(gtffile)
#print(out_type)

files <- grep(fileNameSubstr,list.files(directory),value=TRUE)
condition <- sub(fileNameSubstr,"",files)
table <- data.frame(sampleName = files, fileName = files, condition = condition)

txdb <- makeTxDbFromGFF(gtffile, format="gtf", circ_seqs=character())
ebg <- exonsBy(txdb, by="gene")

ddsHTSeq <- DESeqDataSetFromHTSeqCount(sampleTable = table, directory = directory, design= ~ condition)
rowRanges(ddsHTSeq) <- ebg[intersect(rownames(ddsHTSeq), names(ebg))]
# previously 'rowRanges(ddsHTSeq) <- ebg' worked
ddsHTSeq <- ddsHTSeq[ rowSums(counts(ddsHTSeq)) > 1,]

print("calculate measures of gene expression")

fpkm <- fpkm(ddsHTSeq)
tpm <- apply(fpkm,2,fpkmToTpm)

convert_file_name_to_sample_name <- function(fileName)
{
	sub(fileNameSubstr,"",fileName)
}

convert_gene_version_ENSEMBL_to_gene <- function(geneWithVersion)
{
	unlist(strsplit(geneWithVersion, '\\.'))[c(TRUE,FALSE)]
}

transposed_counts <- t(counts(ddsHTSeq))

print("format row and column names appropriately")
vectorOfSampleNames <- unlist(lapply(rownames(transposed_counts), convert_file_name_to_sample_name))
vectorOfGeneNamesENSEMBL <- unlist(lapply(colnames(transposed_counts), convert_gene_version_ENSEMBL_to_gene))
colnames(transposed_counts) <- vectorOfGeneNamesENSEMBL
rownames(transposed_counts) <- vectorOfSampleNames

transposed_tpm <- t(tpm)
colnames(transposed_tpm) <- vectorOfGeneNamesENSEMBL
rownames(transposed_tpm) <- vectorOfSampleNames

transposed_fpkm <- t(fpkm)
colnames(transposed_fpkm) <- vectorOfGeneNamesENSEMBL
rownames(transposed_fpkm) <- vectorOfSampleNames

genesENSEMBL <- vectorOfGeneNamesENSEMBL
ensembl2entrez <- as.list(org.Hs.egENSEMBL2EG)

print("convert ensembl gene IDs to entrezIDs")
entrezIDsWithNULLs <- sapply(
        genesENSEMBL,
        function(x) ensembl2entrez[[x]][1]
)

print("select only genes with defined entrezIDs")
rownames <- vectorOfSampleNames
colnames <- unlist(lapply(names(entrezIDsWithNULLs), function(x){
			if (!is.null(entrezIDsWithNULLs[[as.character(x)]])){
				entrezIDsWithNULLs[[as.character(x)]]
			}
}))


print("create and store output matrix")

if (out_type=="counts"){
	transposed_counts_entrez_only <- matrix(
                unlist(lapply(names(entrezIDsWithNULLs), function(x){
                                        if (!is.null(entrezIDsWithNULLs[[as.character(x)]])){
                                                transposed_counts[,as.character(x)]
                                        }
                })),
                ncol=length(colnames),
                nrow=length(rownames)
	)
	rownames(transposed_counts_entrez_only) <- rownames
	colnames(transposed_counts_entrez_only) <- colnames

	write.table(
		transposed_counts_entrez_only,
		file=paste(directory,"counts.readyForClassifier.DO_NOT_USE_FOR_CLASSIFICATION.tsv",sep=""),
		row.names=TRUE,
		col.names=TRUE,
		sep="\t",
		quote=FALSE
	)
}

if (out_type=="cpm"){
	transposed_counts_entrez_only <- matrix(
		unlist(lapply(names(entrezIDsWithNULLs), function(x){
					if (!is.null(entrezIDsWithNULLs[[as.character(x)]])){
						transposed_counts[,as.character(x)]
					}
		})),
		ncol=length(colnames),
		nrow=length(rownames)
	)
	rownames(transposed_counts_entrez_only) <- rownames
	colnames(transposed_counts_entrez_only) <- colnames

	counts_entrez_only<-t(transposed_counts_entrez_only)

	d <- DGEList(counts=counts_entrez_only)
	d <- calcNormFactors(d, method="RLE")
	d <- estimateCommonDisp(d, verbose=F)
	d <- estimateTagwiseDisp(d)
	countsnorm = cpm(d, log=FALSE)

	print("print output matrix")

	write.table(
		t(countsnorm),
		file=paste(directory,"cpm.readyForClassifier.tsv",sep=""),
		row.names=TRUE,
		col.names=TRUE,
		sep="\t",
		quote=FALSE
	)

}


if (out_type=="tpm"){
	transposed_tpm_entrez_only <- matrix(
		unlist(lapply(names(entrezIDsWithNULLs), function(x){
					if (!is.null(entrezIDsWithNULLs[[as.character(x)]])){
						transposed_tpm[,as.character(x)]
					}
		})),
		ncol=length(colnames),
		nrow=length(rownames)
	)

	rownames(transposed_tpm_entrez_only) <- rownames
	colnames(transposed_tpm_entrez_only) <- colnames

	write.table(
		transposed_tpm_entrez_only, 
		file=paste(directory,"tpm.readyForClassifier.tsv",sep=""), 
		row.names=TRUE, 
		col.names=TRUE, 
		sep="\t", 
		quote=FALSE
	)
}



if (out_type=="fpkm"){
	transposed_fpkm <- t(fpkm)
	colnames(transposed_fpkm) <- as.character(unlist(strsplit(colnames(transposed_fpkm), '\\.'))[c(TRUE,FALSE)])

	rownames <- unlist(strsplit(rownames(transposed_fpkm), '\\.'))[c(TRUE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE)]
	colnames <- unlist(lapply(names(entrezIDsWithNULLs), function(x){
				if (!is.null(entrezIDsWithNULLs[[as.character(x)]])){
					entrezIDsWithNULLs[[as.character(x)]]
				}
	}))

	transposed_fpkm_entrez_only <- matrix(
		unlist(lapply(names(entrezIDsWithNULLs), function(x){
				if (!is.null(entrezIDsWithNULLs[[as.character(x)]])){
					transposed_fpkm[,as.character(x)]
				}
		})),
		ncol=length(colnames),
		nrow=length(rownames(transposed_fpkm))
	)

	rownames(transposed_fpkm_entrez_only) <- rownames
	colnames(transposed_fpkm_entrez_only) <- colnames

	write.table(
		transposed_fpkm_entrez_only, 
		file=paste(directory,"fpkm.readyForClassifier.tsv",sep=""), 
		row.names=TRUE, 
		col.names=TRUE, 
		sep="\t", 
		quote=FALSE
	)
}

