#!/bin/bash

###
# WARNING: This file is not meant to be executed as a shell script, 
# it is meant to be a list of executed commands which have to be run 
# in a particular order and the command number n is supposed to be run 
# only after the command number n-1 successfully finished.
###

source ./config

###
# index human genome by STAR
###

bsub -cwd ${SCRIPTS} \
        -o ${SCRIPTS}/index_genome_STAR.%J.stdout \
        -e ${SCRIPTS}/index_genome_STAR.%J.stderr \
        ${SCRIPTS}/run_index_STAR.sh

        
###        
# clean data
###

bsub -cwd ${SCRIPTS} \
        -o ${SCRIPTS}/clean.adapt.q20.l50.%J.stdout \
        -e ${SCRIPTS}/clean.adapt.q20.l50.%J.stderr \
        ${SCRIPTS}/run_clean_data.sh

        
###
# map the cleaned data to the indexed genome
###

bsub -cwd ${SCRIPTS} \
        -o ${SCRIPTS}/STAR_mapping.%J.stdout \
        -e ${SCRIPTS}/STAR_mapping.%J.stderr \
        ${SCRIPTS}/run_map_to_reference_STAR.sh

        
###
# get rid of PCR&optical duplicates, 
# merge lanes for each sample, 
# and count reads for all the samples
###

bsub -cwd ${SCRIPTS} \
        -o ${SCRIPTS}/dedup_merge_bams_count_all_samples.%J.stdout \
        -e ${SCRIPTS}/dedup_merge_bams_count_all_samples.%J.stderr \
        ${SCRIPTS}/run_dedup_merge_bams_count_all_samples.sh

        
###        
# copy read counts for calculating gene expression profiles 
# into a separate folder
###

mkdir ${ANALYSIS}/classified_read_counts_from_reference_mapping
cp \
	${ANALYSIS}/CA7AUANXX-2041-*/merged-alns/CA7AUANXX-2041-*.uniq.dedup.tTranscript.mNonempty.sNo.havana.counts \
	${ANALYSIS}/classified_read_counts_from_reference_mapping/


###
# classify read-count based CRC expression profiles 
# to CMS subtypes and join all the classification 
# results together
###

bsub -cwd ${SCRIPTS} \
	-o ${SCRIPTS}/read_counts_to_CMSclasses.%J.stdout \
	-e ${SCRIPTS}/read_counts_to_CMSclasses.%J.stderr \
	${SCRIPTS}/read_counts_to_CMSclasses.sh


##zipping data for WGS metatranscriptomics (Kraken)
#gzip ${ANALYSIS}/CA7AUANXX-2041-*/**/mapping-STAR/*Unmapped.out.mate*
