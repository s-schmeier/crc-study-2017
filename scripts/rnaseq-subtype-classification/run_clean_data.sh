#!/bin/bash

source ./config

bash clean_data.sh \
	-f ${RAW_DATA} \
	-o ${ANALYSIS} \
	-a ./contams_forward_rev.fa \
	-p 0.01 \
	-q 20 \
	-l 50