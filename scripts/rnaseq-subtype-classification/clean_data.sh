#!/bin/bash

#
# script to clean all raw hiSeq demultiplexed fastq sequences
# from one folder.
#

usage(){
	echo "Usage:"
	echo "bash $0 \ "
	echo "-f <archiveFastqFolder> \ "
	echo "-o <outDirWhereSubdirStructureWillBeCreated> \ "
	echo "-a ./contams_forward_rev.fa \ "
	echo "-p 0.01 \ "
	echo "-q 20 \ "
	echo "-l 50"
	}

while getopts ":f:o:a:p:q:l:h" OPT; do
	case $OPT in
		f) folder=$OPTARG
		;;
		o) OUTdir=$OPTARG
		;;
		a) adaptors=$OPTARG
		;;
		p) p=$OPTARG
		;;
		q) qual=$OPTARG
		;;
		l) len=$OPTARG
		;;
		h) usage
		exit 0
		;;
		\?) #unrecognized option - show usage
		echo -e "\nOption -$OPTARG not allowed.\n"
		usage
		exit 2
		;;
	esac
done

source ./utils.sh

declare -a sampleIDs
declare -a laneIDs

echo "#!/bin/bash"
echo "# Script $0 starts: "$(date)

echo "# folder=${folder}"
echo "# OUTdir=${OUTdir}"
echo "# adaptors=${adaptors}"
echo "# qual=${qual}"
echo "# p=$p"
echo "# len=${len}"

basenameList=$(pattern_to_file_basename_list "${folder}/*fastq.gz")

echo "# basenameList=${basenameList}"
echo

# up to date fastq file naming convention as to Nov 2016
flowcellID=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 1 -d ' ' | sort | uniq)
projectID=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 2 -d ' ' | sort | uniq)
sampleIDs=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 3 -d ' ' | sort | uniq)
libraryTypeCode=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 4 -d ' ' | sort | uniq)
libraryVersion=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 5 -d ' ' | sort | uniq)
sampleSheetIDs=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 6 -d ' ' | sort | uniq)
laneIDs=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 7 -d ' ' | sort | uniq)
suffix=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_\|\./ /g' | cut -f 9 -d ' ' | sort | uniq)

process_command "mkdir -p ${OUTdir}" 0 -

#testing loop
#for sample in 01 02 03; do
#	for lane in L002 L003; do

#full-scale run
for sample in ${sampleIDs[@]}; do
	for lane in ${laneIDs[@]}; do

                # fileNamePrefix: prefix up to readIDs (R1/R2) of the files that going to be processed (readID not included)
                # fileNameSuffix: suffix from readID to the end of the fileNames (readID not included)
                # standard file names passed to the script clean_one_dataset.sh have the following form:
                # R1(forward reads): ${flowcellID}-${projectID}-${sample}-${libraryTypeCode}-${libraryVersion}_${sampleSheetID}_${lane}_R1_${suffix}.fastq.gz
                # R2(reverse reads): ${flowcellID}-${projectID}-${sample}-${libraryTypeCode}-${libraryVersion}_${sampleSheetID}_${lane}_R2_${suffix}.fastq.gz

                # warning: further code in clean_one_dataset assumes the last character in fileNamePrefix to be '_'

		list=$(pattern_to_file_basename_list "${folder}/${flowcellID}-${projectID}-${sample}-${libraryTypeCode}-${libraryVersion}_*_${suffix}.fastq.gz")

		sampleSheetID=$(echo ${list} | sed -r 's/\s+/\n/g' |  sed -e 's/-\|_/ /g' | cut -f 6 -d ' ' | sort | uniq)
		fileNamePrefix="${flowcellID}-${projectID}-${sample}-${libraryTypeCode}-${libraryVersion}_${sampleSheetID}_${lane}_"
		fileNameSuffix="_${suffix}.fastq.gz"

		process_command "wait_until_job_slot_available $(whoami) 6" 0 -
                process_command "bsub -cwd $(pwd) \
                       	-o clean_one_dataset.%J.stdout \
                       	-e clean_one_dataset.%J.stderr \
                        ./clean_one_dataset.sh \
                                -f ${folder} -o ${OUTdir}/${flowcellID}-${projectID}-${sample} \
                               	-p ${p} -q ${qual} -l ${len} -a ${adaptors} \
                               	-P ${fileNamePrefix} -S ${fileNameSuffix}" 0 -
		process_command "sleep 1" 0 -
	done
done

echo "# Script $0 ends: "$(date)
