#!/bin/bash

source ./config

bash dedup_merge_bams_count_all_samples.sh \
        -p "${RAW_DATA}/*fastq.gz" \
        -f ${ANALYSIS} \
        -t STAR \
	-a ${HG_ANNOTATION_HAVANA_ONLY} \
	-T NO

