#!/bin/bash

# the function takes input command $1,
# executes it,
# prints the command to $3 logfile or to stdout, if $3 == "-"
# checks exit status after the command execution
# exits script $0 if the exit status isn't the same as $2
process_command(){
	local cmd="$1"
	local successful_exit_status="$2"
	local logFile="$3"

	#print the command to a logfile
	if [ "${logFile}" = "-" ]; then
		echo "${cmd}"
	else
		echo "${cmd}" >> ${logFile}
	fi

	#execute the command
        eval "${cmd}"

        exit_status=$?
	#test exit status
        if test ${exit_status} -ne ${successful_exit_status}
        then
		echo "# The command exits with exit status ${exit_status}; logfile: ${logFile}."
                exit 42
        fi
}


# create list of basenames withouth paths from array $1, discard the path
pattern_to_file_basename_list(){
#	echo "$@"
	OIFS="$IFS"
	IFS=' '
	declare -a l
	local l=($(echo "$1"))
	IFS="$OIFS"

	basenameList=""

	for f in ${l[@]}; do
		basenameList=${basenameList}" "${f##*/}
	done

	basenameList=$(echo ${basenameList} | sed 's/^ //g')
	echo "${basenameList}"
}

# Usage:
#echo $(pattern_to_file_basename_list "../sequences/NZGL01983_C9EP8ANXX/test_analysis/C9EP8ANXX-1983-01-4-1_L001/lengthsort/*.paired.gz")
#echo $(pattern_to_file_basename_list "/archive/nzgl01983/NZGL01983_C9EP*/Raw/*fastq.gz")

# wait until a job slot is available for a user before submitting next job
# (to avoid stacking pending jobs in a queue because that is bad as other
# users cannot get their jobs done)
wait_until_job_slot_available(){
	local user="$1"
	local maxNoJobs="$2"

	if test $(bjobs -w | grep ${user} | wc -l) -ge ${maxNoJobs}; then
		sleep 60
		while [ $(bjobs -w | grep ${user} | wc -l) -ge ${maxNoJobs} ]; do
			sleep 60
		done
	fi
}


exclude_samples(){
	local all=$(echo "$1")
	local exclude=($(echo "$2"))

	for e in ${exclude[@]}; do
		all=$(echo ${all} | sed -r 's/\s+/\n/g' | grep -v ${e} | tr "\n" " ")
	done

	echo ${all}
}

