#!/bin/bash

usage(){
	echo "Usage:"
	echo "bash $0 \ "
	echo "-p <patternForFileNameParsing> \ "
	echo "-f <analysisFolder> \ "
	echo "-t <mappingTool> \ "
	echo "-a <annotation> \ "
	echo "-T <testRun> [YES/NO] "
}

while getopts ":p:f:t:a:T:h" OPT; do
	case $OPT in
		p) PATTERN="$OPTARG"
		;;
		f) FOLDER="$OPTARG"
		;;
		t) TOOL="$OPTARG"
		;;
		a) annotation="$OPTARG"
		;;
		T) testRun="$OPTARG"
		;;
		h) usage
		exit 0
		;;
		\?) #unrecognized option - show usage
		echo -e "\nOption -$OPTARG not allowed.\n"
		usage
		exit 2
		;;
	esac
done

source ./utils.sh

echo "#!/bin/bash"
echo "# Script $0 starts: "$(date)
echo

echo "# PATTERN=${PATTERN}"
echo "# FOLDER=${FOLDER}"
echo "# TOOL=${TOOL}"
echo "# annotation=${annotation}"
echo "# testRun=${testRun}"

basenameList=$(pattern_to_file_basename_list "${PATTERN}")

echo "# basenameList=${basenameList}"
echo

flowcellID=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 1 -d ' ' | sort | uniq)
projectID=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 2 -d ' ' | sort | uniq)
sampleIDs=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 3 -d ' ' | sort | uniq)
libraryTypeCode=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 4 -d ' ' | sort | uniq)
libraryVersion=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 5 -d ' ' | sort | uniq)
sampleSheetIDs=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 6 -d ' ' | sort | uniq)
laneIDs=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_/ /g' | cut -f 7 -d ' ' | sort | uniq)
suffix=$(echo ${basenameList} | sed -r 's/\s+/\n/g' | sed -e 's/-\|_\|\./ /g' | cut -f 9 -d ' ' | sort | uniq)


b=$(basename ${annotation})
str=$(echo ${b%.*})
echo "# str=${str}"

samples=""
lanes=""
if [ "${testRun}" = "YES" ]; then
	samples="01 02 03"
	lanes="L002 L003"
else
	samples=${sampleIDs}
	lanes=${laneIDs}
fi

for sample in ${samples}; do
	markDuplicatesOptions=""
	for lane in ${lanes}; do

             	list=$(pattern_to_file_basename_list "${PATTERNfolder}/${flowcellID}-${projectID}-${sample}-${libraryTypeCode}-${libraryVersion}_*_${suffix}.fastq.gz")

                sampleSheetID=$(echo ${list} | sed -r 's/\s+/\n/g' |  sed -e 's/-\|_/ /g' | cut -f 6 -d ' ' | sort | uniq)
                fileNamePrefix="${flowcellID}-${projectID}-${sample}-${libraryTypeCode}-${libraryVersion}_${sampleSheetID}_${lane}_"
                fileNameSuffix='.noAdapt_${suffix}.fastq.trimmed.paired.gz'

                shortID=${flowcellID}-${projectID}-${sample}
                bam=$(ls ${FOLDER}/${shortID}/${fileNamePrefix%_}/mapping-${TOOL}/${fileNamePrefix%_}.sortedByCoord.${TOOL}*.bam)

                markDuplicatesOptions=${markDuplicatesOptions}" I="${bam}
        done

	markDuplicatesOptions=$(echo ${markDuplicatesOptions} | sed 's/^ //g')

	echo
        echo "# markDuplicatesOptions=${markDuplicatesOptions}"

        OUTfolder=${FOLDER}/${shortID}/merged-alns
        process_command "mkdir -p ${OUTfolder}" 0 -

	process_command "wait_until_job_slot_available $(whoami) 6" 0 -
        process_command "bsub -cwd $(pwd) \
                -o dedup_merge_count_one_sample.${TOOL}.${str}.%J.stdout \
                -e dedup_merge_count_one_sample.${TOOL}.${str}.%J.stderr \
		./dedup_merge_bams_count_one_sample.sh \
                        -m \"${markDuplicatesOptions}\" \
                        -o ${OUTfolder} \
                        -n ${shortID} \
			-a ${annotation}" 0 -
	process_command "sleep 1" 0 -
done

echo
echo "# Script $0 ends: "$(date)

