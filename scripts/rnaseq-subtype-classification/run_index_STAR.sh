#!/bin/bash

source ./config

bash index_STAR.sh \
	-r ${HG_REFERENCE} \
	-d ${HG_INDEX} \
	-o "--runThreadN 4 --sjdbGTFfile ${HG_ANNOTATION} --sjdbOverhang 100"