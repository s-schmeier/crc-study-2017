#!/bin/bash

source ./config

module load ${R_MODULE}

TYPES=("counts" "cpm" "tpm" "fpkm")

DATA="${ANALYSIS}/classified_read_counts_from_reference_mapping/"
DATA2="${ANALYSIS}/classified_read_counts_from_kmer_pseudomapping/"

#generate input suitable for CMS classifier
for TYPE in ${TYPES[@]}; do
        echo "summarising and transforming counts to $TYPE: " $(date)
        R --vanilla --slave --args\
                ${DATA} \
                .uniq.dedup.tTranscript.mNonempty.sNo.havana.counts \
                /databases/igenomes/Homo_sapiens/NCBI/GRCh38/Annotation/Genes.gencode/genes.gtf \
                ${TYPE} \
                < ${SCRIPTS}/summarize_and_transform_counts.R \
                >> ${SCRIPTS}/summarize_and_transform_counts.${TYPE}.log 2>&1
done

#use only cpm tpm and fpkm for further calculations
TYPES=("cpm" "tpm" "fpkm")

for TYPE in ${TYPES[@]}; do
        echo "classify samples with CMSclassifier, $TYPE: " $(date)
        R --vanilla --slave --args \
                ${DATA}/${TYPE}.readyForClassifier.tsv \
                ${DATA}/CMSclassifiedCRC.${TYPE}.tsv \
                < ${SCRIPTS}/classify_samples_with_CMSclassifier.R \
                >> ${SCRIPTS}/classify_samples_with_CMSclassifier.${TYPE}.log 2>&1

        echo "join classes and logNormExpression values, $TYPE: " $(date)
        R --vanilla --slave --args \
                ${DATA}/CMSclassifiedCRC.${TYPE}.tsv \
                ${DATA}/${TYPE}.readyForClassifier.tsv \
                ${DATA}/CMSclasses_logNormExprLevels.${TYPE}.tsv \
                < ${SCRIPTS}/join_CMSclasses_logNormExpr.R \
                >> ${SCRIPTS}/join_CMSclasses_logNormExpr.${TYPE}.log 2>&1
done

R --vanilla --slave \
        --args ${DATA}/../summaryCMSclassified.tsv \
        fpkm,tpm,cpm\
        ${DATA}/CMSclassifiedCRC.fpkm.tsv \
        ${DATA}/CMSclassifiedCRC.tpm.tsv \
        ${DATA}/CMSclassifiedCRC.cpm.tsv \
        < ${SCRIPTS}/summarize_CMSclasses.R \
        >> ${SCRIPTS}/summarize_CMSclasses.log 2>&1

module unload R
