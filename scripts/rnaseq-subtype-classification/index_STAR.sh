#!/bin/bash

#
# script index reference sequence for bowtie2
#

usage(){
        echo -e "Usage:
        bash $0
        -r <reference_assembly.fasta>
	-d <output_directory>
	-o <additional_options_to_be_passed_to_STAR>
	> log 2> err"
        }

while getopts ":r:d:o:h" OPT; do
        case $OPT in
                r) FASTA_REFERENCE=$OPTARG
                   ;;
		d) INDEXfolder=$OPTARG
		   ;;
		o) ADDITIONALoptions=$OPTARG
		   ;;
		h) usage
                   exit 0
                   ;;
                \?) #unrecognized option - show usage
                    echo -e "\nOption -$OPTARG not allowed.\n"
                    usage
                    exit 2
                    ;;
        esac
done

# the function takes input command $1,
# executes it,
# prints the command to stdout
# checks exit status after the command execution
# exits script $0 if the exit status isn't the same as $2
process_command(){
        cmd=$1
	successful_exit_status=$2

       	#print the command to stdout
        echo ${cmd}

       	#execute the command
        eval ${cmd}

        exit_status=$?
       	#test exit status
        if test ${exit_status} -ne ${successful_exit_status}
        then
            	echo "# The command exits with exit status ${exit_status}."
                exit 42
        fi
}

echo "#!/bin/bash"
echo "# Script $0 starts: "$(date)

echo "# FASTA_REFERENCE=${FASTA_REFERENCE}"
echo "# INDEXfolder=${INDEXfolder}"
echo "# ADDITIONALoptions=${ADDITIONALoptions}"
echo
echo "# STAR version: "$(STAR --version)
echo

# create tmp folder
TMPfolder=$(mktemp -d /tmp/STAR_index.XXXXXXXXX)
echo 'TMPfolder=$(mktemp -d /tmp/STAR_index.XXXXXXXXX)'

# set variables
fasta_name=$(basename ${FASTA_REFERENCE})
fasta_noExt=${fasta_name%.*}
#fasta_folder=${FASTA_REFERENCE%/*}

echo "# fasta_name=${fasta_name}"
echo "# fasta_noExt=${fasta_noExt}"
#echo "# fasta_folder=${fasta_folder}"

process_command "cp ${FASTA_REFERENCE} ${TMPfolder}/${fasta_name}" 0
process_command "mkdir -p ${INDEXfolder}" 0

echo
echo "# Indexing genome"
process_command "STAR \
	--runMode genomeGenerate \
	${ADDITIONALoptions} \
	--genomeDir ${TMPfolder} \
	--genomeFastaFiles ${TMPfolder}/${fasta_name} \
	> ${INDEXfolder}/${fasta_noExt}.stdout \
	2> ${INDEXfolder}/${fasta_noExt}.stderr" 0

echo
echo "# Move content out of /tmp folder"
process_command "rm -f ${TMPfolder}/${fasta_name}" 0
process_command "mv -u ${TMPfolder}/* ${INDEXfolder}" 0

echo
echo "# Script $0 ends: "$(date)
