#!/bin/bash

source ./config

bash map_to_reference.sh \
	-r ${HG_REFERENCE} \
        -i ${HG_INDEX} \
        -f ${ANALYSIS} \
	-p "${RAW_DATA}/*fastq.gz" \
        -t STAR \
	-o "--runThreadN 4 \
		--limitBAMsortRAM 20000000000 \
		--genomeLoad LoadAndRemove \
		--outFilterMultimapNmax 20 \
		--outSAMtype BAM SortedByCoordinate \
		--outFilterType BySJout \
		--alignSJoverhangMin 8 \
		--alignSJDBoverhangMin 1 \
		--outFilterMismatchNmax 999 \
		--alignIntronMin 20 \
		--alignIntronMax 1000000 \
		--alignMatesGapMax 1000000 \
		--quantMode GeneCounts \
		--outReadsUnmapped Fastx \
		--outFilterMatchNminOverLread 0.4 \
		--outFilterScoreMinOverLread 0.4" \
	-R YES \
	-T NO