#!/bin/bash

##
# picard tools to markduplicates,
# merge files from multiple lanes,
# samtools to take only uniquely mapping reads,
# and htseq-count to count expression levels across features (genes)
##

usage(){
	echo "Usage:"
	echo "bash $0 \ "
	echo "-m <markDuplicatesOptions> \ "
	echo "-o <outFolder> \ "
	echo "-n <outNamePrefix> \ "
	echo "-a <annotation>"
}

while getopts ":m:o:n:a:h" OPT; do
	case $OPT in
		m) markDuplicatesOptions="$OPTARG"
		;;
		o) outFolder="$OPTARG"
		;;
		n) outNamePrefix="$OPTARG"
		;;
		a) annotation="$OPTARG"
		;;
		h) usage
		exit 0
		;;
		\?) #unrecognized option - show usage
		echo -e "\nOption -$OPTARG not allowed.\n"
		usage
		exit 2
		;;
	esac
done


source ./utils.sh

b=$(basename ${annotation})
str=$(echo ${b%.*})

log=${outFolder}/../dedup_merge_bams_count.${outNamePrefix}.${str}.tTranscript.mNonempty.sNo.log

echo > ${log}
echo "# Script $0 starts: "$(date) >> ${log}
echo >> ${log}

echo "# markDuplicatesOptions=${markDuplicatesOptions}" >> ${log}
echo "# outFolder=${outFolder}" >> ${log}
echo "# outNamePrefix=${markDuplicatesOptions}" >> ${log}
echo "# annotation=${annotation}"

echo >> ${log}

echo "# picard version: "$(picard MarkDuplicates --version) >> ${log}
echo >> ${log}

process_command "picard MarkDuplicates \
	${markDuplicatesOptions} \
	O=${outFolder}/${outNamePrefix}.dup_marked.merged.bam \
	M=${outFolder}/${outNamePrefix}.dup_marked.metrix.txt \
	2> ${outFolder}/${outNamePrefix}.picard_markDuplicates.stderr" 0 ${log}


# using the most recent version nowadays
module load samtools/1.3.1
echo "# which samtools: "$(which samtools) >> ${log}
echo "# samtools: "$(samtools) >> ${log}
echo >> ${log}

process_command "samtools view -h -q 255 -b -F 1024 \
	${outFolder}/${outNamePrefix}.dup_marked.merged.bam \
	> ${outFolder}/${outNamePrefix}.uniq.dedup.bam \
	2> ${outFolder}/${outNamePrefix}.dup_marked.merged.samtools_view.stderr" 0 ${log}

process_command "samtools index -b \
	${outFolder}/${outNamePrefix}.uniq.dedup.bam \
	2> ${outFolder}/${outNamePrefix}.uniq.dedup.samtools_index.stderr" 0 ${log}

process_command "samtools index -b \
        ${outFolder}/${outNamePrefix}.dup_marked.merged.bam \
        2> ${outFolder}/${outNamePrefix}.dup_marked.merged.samtools_index.stderr" 0 ${log}

module unload samtools

echo "# htseq-count version: "$(htseq-count | grep version) >> ${log}
echo >> ${log}

echo >> ${log}
echo "# str="${str} >> ${log}

process_command "htseq-count -f bam -r pos \
	-m intersection-nonempty -s no \
	-t transcript \
	${outFolder}/${outNamePrefix}.uniq.dedup.bam \
	${annotation} \
	> ${outFolder}/${outNamePrefix}.uniq.dedup.tTranscript.mNonempty.sNo.${str}.counts \
	2> ${outFolder}/${outNamePrefix}.uniq.dedup.tTranscript.mNonempty.sNo.${str}.counts.err" 0 ${log}

process_command "htseq-count -f bam -r pos \
	-m intersection-nonempty -s no \
	-t transcript \
	${outFolder}/${outNamePrefix}.dup_marked.merged.bam \
	${annotation} \
	> ${outFolder}/${outNamePrefix}.dup_marked.merged.tTranscript.mNonempty.sNo.${str}.counts \
	2> ${outFolder}/${outNamePrefix}.dup_marked.merged.tTranscript.mNonempty.sNo.${str}.counts.err" 0 ${log}

echo >> ${log}
echo "# Script $0 ends: "$(date) >> ${log}
