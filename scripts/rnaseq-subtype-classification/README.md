# CRC processing RNAseq reads to CMS subtypes

This folder contains scripts used for processing of tumour tissue RNAseq data for this CRC study: 
[doi:10.1038/s41598-017-11237-6](https://www.nature.com/articles/s41598-017-11237-6).

The most high level script is `pipeline.sh`, listing commands that need to be run consecutively 
in order to process raw RNAseq reads from the study to CMS subtype for each sample.

Paths to locations where the data were stored on our compute, are stored in the file `config`.


## Dependencies

1. LSF
2. fastq-mcf (from EA Utils)	v1.1.2.537
3. SolexaQA++	v3.1.6
4. samtools	v1.3.3
5. STAR	2.5.2b
6. htseq-count	v0.6.1p1
7. DESeq2	v1.10.1
8. CMSclassifier	v1.0.0
9. R	3.3.3
