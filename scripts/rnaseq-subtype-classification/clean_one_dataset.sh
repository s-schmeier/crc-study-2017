#!/bin/bash

#
# script to clean data for one lane of one sample (HiSeq)
#

usage(){
        echo "Usage:"
        echo "bash $0 \ "
        echo "-f <rawFastqFolderInArchive> \ "
        echo "-o <outDirWhereSubdirStructureWillBeCreated> \ "
        echo "-p <maxReadErrorProbability> -q <correspondingPhred33QualityScore> \ "
        echo "-l <minReadLength> -a <adaptors> \ "
        echo "-P <fastqFilePrefixToReadID-R1/R2> -S <fastqFileSuffixAfterReadID-R1/R2> "
}

while getopts ":f:o:p:q:l:a:P:S:h" OPT; do
        case $OPT in
                f) folder=$OPTARG
                ;;
                o) OUTdir=$OPTARG
                ;;
                p) p=$OPTARG
                ;;
                q) qual=$OPTARG
                ;;
                l) len=$OPTARG
                ;;
                a) adaptors=$OPTARG
                ;;
                P) fileNamePrefix=$OPTARG
                ;;
                S) fileNameSuffix=$OPTARG
                ;;
                h) usage
                exit 0
                ;;
                \?) #unrecognized option - show usage
                echo -e "\nOption -$OPTARG not allowed.\n"
                usage
                exit 2
                ;;
        esac
done

source ./utils.sh

fileNameSuf=${fileNameSuffix%.*}
log=${OUTdir}/${fileNamePrefix%_}/data_cleaning.${fileNamePrefix%_}.log
process_command "mkdir -p ${OUTdir}/${fileNamePrefix%_}" 0 -
folderA=${OUTdir}/${fileNamePrefix%_}/analysis
folderNoAdapt=${OUTdir}/${fileNamePrefix%_}/noAdapt
folderNoAdaptQ=${OUTdir}/${fileNamePrefix%_}/dynamictrim
folderNoAdaptQL=${OUTdir}/${fileNamePrefix%_}/lengthsort

echo > ${log}
echo >> ${log}
echo "# Processing sample ${fileNamePrefix%_} by script $0 starts: "$(date) >> ${log}
echo >> ${log}

#
# print parameters to log
#

echo "# folder=${folder}" >> ${log}
echo "# OUTdir=${OUTdir}" >> ${log}
echo "# p=${p}" >> ${log}
echo "# q=${q}" >> ${log}
echo "# len=${len}" >> ${log}
echo "# adaptors=${adaptors}" >> ${log}
echo "# fileNamePrefix=${fileNamePrefix}" >> ${log}
echo "# fileNameSuffix=${fileNameSuffix}" >> ${log}
echo "# fileNameSuf=${fileNameSuf}" >> ${log}
echo "# log=${log}" >> ${log}

#
# create new output folders
#

process_command "mkdir -p ${folderA}" 0 ${log}
process_command "mkdir -p ${folderNoAdapt}" 0 ${log}
process_command "mkdir -p ${folderNoAdaptQ}" 0 ${log}
process_command "mkdir -p ${folderNoAdaptQL}" 0 ${log}

#
# create tmp folders
#

TMPfolderFQ=$(mktemp -d /tmp/fastq.XXXXXXXXX)
TMPfolderA=$(mktemp -d /tmp/analysis.XXXXXXXXX)
TMPfolderNoAdapt=$(mktemp -d /tmp/noAdapt.XXXXXXXXX)
TMPfolderNoAdaptQ=$(mktemp -d /tmp/dynamictrim.XXXXXXXXX)
TMPfolderNoAdaptQL=$(mktemp -d /tmp/lengthsort.XXXXXXXXX)

echo 'TMPfolderFQ=$(mktemp -d /tmp/fastq.XXXXXXXXX)' >> ${log}
echo 'TMPfolderA=$(mktemp -d /tmp/analysis.XXXXXXXXX)' >> ${log}
echo 'TMPfolderNoAdapt=$(mktemp -d /tmp/noAdapt.XXXXXXXXX)' >> ${log}
echo 'TMPfolderNoAdaptQ=$(mktemp -d /tmp/dynamictrim.XXXXXXXXX)' >> ${log}
echo 'TMPfolderNoAdaptQL=$(mktemp -d /tmp/lengthsort.XXXXXXXXX)' >> ${log}

echo >> ${log}
echo "# Unzip raw fastq files for fastq-mcf adapter trimming: "$(date) >> ${log} 
for r in R1 R2; do
	process_command "cp ${folder}/${fileNamePrefix}${r}${fileNameSuffix} \
		${TMPfolderFQ}/${fileNamePrefix}${r}${fileNameSuffix}" 0 ${log}
	process_command "chmod u+rwx ${TMPfolderFQ}/${fileNamePrefix}${r}${fileNameSuffix}" 0 ${log}
	process_command "gunzip -c ${TMPfolderFQ}/${fileNamePrefix}${r}${fileNameSuffix} \
		> ${TMPfolderFQ}/${fileNamePrefix}${r}${fileNameSuf}" 0 ${log}
done


echo >> ${log}
echo "# SolexaQA++ analysis of raw fastq sequences starts: "$(date) >> ${log}
echo "# "$(SolexaQA++ | grep "^SolexaQA++") >> ${log}
process_command "SolexaQA++ analysis \
	${TMPfolderFQ}/${fileNamePrefix}R1${fileNameSuffix} \
	${TMPfolderFQ}/${fileNamePrefix}R2${fileNameSuffix} \
	-p $p -d ${TMPfolderA} \
	> ${folderA}/${fileNamePrefix%_}.solexaQA.analysis.stdout \
	2> ${folderA}/${fileNamePrefix%_}.solexaQA.analysis.stderr" 0 ${log}


echo >> ${log}
echo "# Adapter trimming starts: "$(date) >> ${log}
echo "# using fastq-mcf from ea-utils 1.1.2.537" >> ${log}
# -- the latest supported version on BioIT, Nov 2016
process_command "fastq-mcf \
	-k 0 \
	-o ${TMPfolderNoAdapt}/${fileNamePrefix}R1.noAdapt${fileNameSuf} \
	-o ${TMPfolderNoAdapt}/${fileNamePrefix}R2.noAdapt${fileNameSuf} \
	${adaptors} \
	${TMPfolderFQ}/${fileNamePrefix}R1${fileNameSuf} \
	${TMPfolderFQ}/${fileNamePrefix}R2${fileNameSuf} \
	> ${folderNoAdapt}/${fileNamePrefix%_}.fastq_mcf.stdout \
	2> ${folderNoAdapt}/${fileNamePrefix%_}.fastq_mcf.stderr" 0 ${log}


echo >> ${log}
echo "# Zip trimmed files: "$(date) >> ${log}
for r in R1 R2; do
	process_command "gzip ${TMPfolderNoAdapt}/${fileNamePrefix}${r}.noAdapt${fileNameSuf}" 0 ${log}
done


#
# this code cannot preccede fastq-mcf nor solexaQA++ initial analysis
# (the files are needed as input for those parts of the code)
#
echo >> ${log}
echo "# Remove (un)zipped raw files: "$(date) >> ${log}
for r in R1 R2; do
	process_command "rm -f ${TMPfolderFQ}/${fileNamePrefix}${r}${fileNameSuf}" 0 ${log}
	process_command "rm -f ${TMPfolderFQ}/${fileNamePrefix}${r}${fileNameSuffix}" 0 ${log}
done


echo >> ${log}
echo "# SolexaQA++ dynamictrim starts: "$(date) >> ${log}
process_command "SolexaQA++ dynamictrim \
	${TMPfolderNoAdapt}/${fileNamePrefix}R1.noAdapt${fileNameSuffix} \
	${TMPfolderNoAdapt}/${fileNamePrefix}R2.noAdapt${fileNameSuffix} \
	-p $p -d ${TMPfolderNoAdaptQ} \
	> ${folderNoAdaptQ}/${fileNamePrefix%_}.noAdapt.solexaQA.dynamictrim.stdout \
	2> ${folderNoAdaptQ}/${fileNamePrefix%_}.noAdapt.solexaQA.dynamictrim.stderr" 0 ${log}


#
# this code cannot preccede solexaQA++ dynamictrim (the files are needed as input)
#
echo >> ${log}
echo "# Remove *noAdapt.fastq.gz files: "$(date)  >> ${log}
for r in R1 R2; do
	process_command "rm -f ${TMPfolderNoAdapt}/${fileNamePrefix}${r}.noAdapt${fileNameSuffix}" 0 ${log}
done


echo >> ${log}
echo "# SolexaQA++ lengthsort starts: "$(date) >> ${log}
process_command "SolexaQA++ lengthsort \
	${TMPfolderNoAdaptQ}/${fileNamePrefix}R1.noAdapt${fileNameSuf}.trimmed.gz \
	${TMPfolderNoAdaptQ}/${fileNamePrefix}R2.noAdapt${fileNameSuf}.trimmed.gz \
	-l ${len} -d ${TMPfolderNoAdaptQL} \
	> ${folderNoAdaptQL}/${fileNamePrefix%_}.noAdapt.trimmed.solexaQA.lengthsort.stdout \
	2> ${folderNoAdaptQL}/${fileNamePrefix%_}.noAdapt.trimmed.solexaQA.lengthsort.stderr" 0 ${log}


#
# this code cannot preccede solexaQA lengthsort (the files are needed as input)
#
echo >> ${log}
echo "# Remove *trimmed.gz files: "$(date) >> ${log}
for r in R1 R2; do
	process_command "rm -f ${TMPfolderNoAdaptQ}/${fileNamePrefix}_${r}_${suffix}.noAdapt.fastq.trimmed.gz" 0 ${log}
done


echo >> ${log}
echo "# Move content out of /tmp folders: "$(date) >> ${log}
process_command "mv ${TMPfolderA}/* ${folderA}" 0 ${log}
process_command "mv ${TMPfolderNoAdaptQ}/* ${folderNoAdaptQ}" 0 ${log}
process_command "mv ${TMPfolderNoAdaptQL}/* ${folderNoAdaptQL}" 0 ${log}
echo >> ${log}


echo "# Script $0 ends: "$(date) >> ${log}
