#!/bin/bash

projectFolder="/active/nzgl02031"
name="wholeDataset"
metafile="${projectFolder}/metaInfo.tsv"
fastaFiles="${projectFolder}/${name}/preprocessedFastaFilenames.txt"

SCRIPTS=$(pwd)

echo "QIIME: $QIIMEdir"
echo "name: ${name}"
echo "metafile: ${metafile}"
echo "fastaFiles: ${fastaFiles}"

${SCRIPTS}/QIIMEanalysis.pl \
        --name ${name} \
        --projectFolder ${projectFolder} \
 	--step 'analysis' \
 	--quality '0.01' \
 	--length '250' \
	--metafile ${metafile} \
	--fastaFiles ${fastaFiles} \
	--subsample 'False' \
        --pickDeNovoOtusOptions '-f' \
	--makeOtuHeatmapOptions '' \
	--summarizeTaxaThroughPlotsOptions '-f' \
        --betaDiversityThroughPlotsOptions '-e 110 -f' \
        --jackknifedBetaDiversityOptions '-e 110 -f' \
        --alphaRarefactionOptions '-f'

echo "QIIMEanalysis.pl exit status: $?"
