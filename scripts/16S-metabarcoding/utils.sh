#!/bin/bash

# the function takes input command $1,
# executes it,
# prints the command to $3 logfile or to stdout, if $3 == "-"
# checks exit status after the command execution
# exits script $0 if the exit status isn't the same as $2
process_command(){
        local cmd="$1"
        local successful_exit_status="$2"
        local logFile="$3"

       	#print the command to a logfile
        if [ "${logFile}" = "-" ]; then
                echo "${cmd}"
        else
            	echo "${cmd}" >> ${logFile}
        fi

       	#execute the command
        eval "${cmd}"

        exit_status=$?
       	#test exit status
        if test ${exit_status} -ne ${successful_exit_status}
        then
            	echo "# The command exits with exit status ${exit_status}; logfile: ${logFile}."
                exit 42
        fi
}

