#!/bin/bash

projectFolder="/active/nzgl02031"
name="wholeDataset"
biom="${projectFolder}/${name}/analysis_q0.01_l250/otus_cat/otu_table.biom"
allSummaries="${projectFolder}/${name}/analysis_q0.01_l250/wf_taxa_summary_cats/"
metafile="metaInfo.tsv"
cats=("Differentiation" "Side" "CMS")

mkdir -p "${projectFolder}/${name}/analysis_q0.01_l250/otus_cat"

pick_de_novo_otus.py \
	-i "${projectFolder}/${name}/preprocess_q0.01_l250/readyForAnalysis_q0.01_l250.fa" \
	-o "${projectFolder}/${name}/analysis_q0.01_l250/otus_cat" \
	-f

filter_samples_from_otu_table.py \
	-i ${biom} \
	-o "${projectFolder}/${name}/analysis_q0.01_l250/otus_cat/CMS_otu_table.biom" \
	-m ${metafile} \
	-s 'CMS:*,!N/A,!RNA_degraded'

for category in ${cats[@]}; do
	echo ${category}
	echo ${allSummaries}${category}
	mkdir -p ${allSummaries}${category}

	if [ "$category" = "CMS" ]; then
		filter_samples_from_otu_table.py \
		       	-i ${biom} \
		       	-o "${projectFolder}/${name}/analysis_q0.01_l250/otus_cat/${category}_otu_table.biom" \
		       	-m ${metafile} \
		       	-s 'CMS:*,!N/A,!RNA_degraded'

		summarize_taxa_through_plots.py \
                        -i "${projectFolder}/${name}/analysis_q0.01_l250/otus_cat/${category}_otu_table.biom" \
                        -o ${allSummaries}${category} \
                        -m ${metafile} \
                        -f -c ${category}

	elif [ "$category" = "Differentiation" ]; then 
                filter_samples_from_otu_table.py \
                        -i ${biom} \
                        -o "${projectFolder}/${name}/analysis_q0.01_l250/otus_cat/${category}_otu_table.biom" \
                        -m ${metafile} \
                        -s 'Differentiation:*,!No'

                summarize_taxa_through_plots.py \
                        -i "${projectFolder}/${name}/analysis_q0.01_l250/otus_cat/${category}_otu_table.biom" \
                        -o ${allSummaries}${category} \
                        -m ${metafile} \
                        -f -c ${category}
	else
		summarize_taxa_through_plots.py \
			-i ${biom} \
			-o ${allSummaries}${category} \
			-m ${metafile} \
			-f -c ${category}
	fi
done
