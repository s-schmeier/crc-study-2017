# CRC 16S metabarcoding README.md

This folder contains scripts used for processing of 16S metabarcoding data for this CRC study: 
[doi:10.1038/s41598-017-11237-6](https://www.nature.com/articles/s41598-017-11237-6).

The most high level script is `pipeline.sh`, listing commands that need to be run consecutively 
in order to process raw 16S metabarcoding reads from the study to taxa abundances per sample 
as well as summary abundances per category.

## Dependencies

1. QIIME v1.9
2. biom v2.1.5
3. FLASh v1.2.11
4. SolexaQA++ v3.1.5
5. usearch61 v6.1.544
