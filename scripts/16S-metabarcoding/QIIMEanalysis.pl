#!/usr/bin/perl

use strict;
use warnings;
use Bio::Seq;
use Bio::SeqIO;
use Bio::Seq::Quality;
use Bio::SeqIO::fastq;
use Getopt::Long;
use Pod::Usage;
use Data::Dumper;
use File::Basename;

my $options={};
$options = set_default_option_values();

GetOptions (
'help'					=> \$options->{'help'},
'name:s'				=> \$options->{'name'},
'projectFolder:s'			=> \$options->{'NZGLroot'},
'step=s'				=> \$options->{'stepString'},
'quality=s'				=> \$options->{'qualString'},
'length=s'				=> \$options->{'lengthString'},
'replicate=s'				=> \$options->{'replicateString'},
'metaCounts=s'				=> \$options->{'metaCounts'},
'metaCountsIDfield=i'			=> \$options->{'metaCountsIDfield'},
'metaCountsReplicateField=i'		=> \$options->{'metaCountsReplicateField'},
'metaCountsFolderField=i'		=> \$options->{'metaCountsFolderField'},
'metaCountsHeader=s'			=> \$options->{'metaCountsHeaderString'},
'flashOptions=s'			=> \$options->{'flashOptions'},
'subsample:s'				=> \$options->{'subsample'},
'subsampleRatio=f'			=> \$options->{'subRatio'},
'subsampleVersion=s'			=> \$options->{'subsampleFunction'},
'metafile=s'				=> \$options->{'metafile'},
'fastaFiles=s'				=> \$options->{'metafileFasta'},	
'pickDeNovoOtusOptions=s'		=> \$options->{'pickDeNovoOtusOptions'},
'biomSummarizeTableOptions=s'		=> \$options->{'biomSummarizeTableOptions'},
'makeOtuHeatmapOptions=s'		=> \$options->{'makeOtuHeatmapOptions'}, 
'makeOtuNetworkOptions=s'		=> \$options->{'makeOtuNetworkOptions'}, 
'summarizeTaxaThroughPlotsOptions=s'	=> \$options->{'summarizeTaxaThroughPlotsOptions'}, 
'betaDiversityThroughPlotsOptions=s'	=> \$options->{'betaDiversityThroughPlotsOptions'}, 
'jackknifedBetaDiversityOptions=s'	=> \$options->{'jackknifedBetaDiversityOptions'},
'alphaRarefactionOptions=s'		=> \$options->{'alphaRarefactionOptions'},
'compareCategories'			=> \$options->{'compareCategories'},
'category=s'				=> \$options->{'catString'},
'compareCategoriesOptions=s'		=> \$options->{'compareCategoriesOptions'},
) or pod2usage(-msg=>"Invalid arguments", -verbose=>1);

pod2usage(-verbose=>2) if $options->{'help'};

#################
#		
#	main	
#		
#################

my ($preprocess, $analysis, $sortedQual, $sortedLength) = set_values($options);
my @outFH=();
push @outFH, \*STDOUT;
my $log = create_name($options->{'NZGLroot'},$options->{'name'},"activityLog.txt",\@outFH);
open (LOG, ">>$log" ) or die ("couldn't open $log: $!\n");
push @outFH, \*LOG;

print_msg_to_filehandles(\@outFH, "Script $0 starts at: ".scalar(localtime));
print_options_setting(\@outFH, $options);
check_options_setting(\@outFH, $options);

my $Fasta={};
 
## do the QIIME work 
## consists of two parts: preprocess and analysis

##
## preprocess data, so that QIIME analysis can be done
##

if ($preprocess){
	print_msg_to_filehandles(\@outFH, "Sequencing data preprocess starts at: ".scalar(localtime));
	$options->{'metafile'} = create_name($options->{'NZGLroot'},$options->{'name'},"metaInfo.txt", \@outFH);
	create_metafile($options->{'metafile'}, $options->{'metaCountsHeaderString'}, $options->{'metaCounts'});
	print_msg_to_filehandles(\@outFH, "Create meta-file $options->{metafile}.");

	if ($options->{'subsample'} eq 'True'){
		my $seed = time ^ $$ ^ unpack "%L*", `ps axww | gzip`;
		srand($seed);
		print_msg_to_filehandles(\@outFH, "Use random seed: $seed");
		# As data processing before subsampling step is not deterministic 
		# (more specifically, QIIME scripts identify_chimeric_seqs.py, and filter_fasta.py), 
		# and previously used seeds are not reported, 
		# this information is more about logging than about reproducibility.  
		# That's why there is not input option '--seed' for now.
	}

	foreach my $curQual ( @{$sortedQual} ){
		foreach my $curLength ( @{$sortedLength} ){
                        # location where preprocessed data are stored 
                        my $preprocessFolders = {};
                        my $preprocessRoot = create_name($options->{'NZGLroot'},$options->{'name'},"preprocess_q".$curQual."_l".$curLength."/", \@outFH);
                        $preprocessFolders = create_folders_preprocess($preprocessRoot, \@outFH);
                        $Fasta->{$curQual}{$curLength} = create_fastafile($preprocessFolders,
		                                                        \@outFH,
	        	                                                $curQual,
        	        	                                        $curLength,
									$options,
       	                                        		        );
                        print_msg_to_filehandles(\@outFH, "Create fasta-file $Fasta->{$curQual}{$curLength}.");
		}
	}

	$options->{'metafileFasta'} = create_name($options->{'NZGLroot'},$options->{'name'},"preprocessedFastaFilenames.txt", \@outFH);
	save_preprocessed_fasta_names($options->{'metafileFasta'}, $sortedQual, $sortedLength, $Fasta, \@outFH);
	print_msg_to_filehandles(\@outFH, "QIIME preparation complete at: ".scalar(localtime));
}

##
## analyse data using QIIME
##

if ($analysis){

	print_msg_to_filehandles(\@outFH, "QIIME analysis starts at: ".scalar(localtime));	

	unless ( -e $options->{'metafile'} ){die "main/ Metafile $options->{metafile} does not exist.\n"};
	print_msg_to_filehandles(\@outFH, "Use metafile $options->{metafile}.");

	#
	# read in a table of fasta filenames from $metafileFasta, if provided as input option, 
	# otherwise use $Fasta defined in the preprocess stage.
	#
	if ( !$preprocess ){
		$Fasta = load_preprocessed_fasta_names($options->{'metafileFasta'}, $sortedQual, $sortedLength, \@outFH);
	}elsif( !%{$Fasta} ){
		die "Filenames of preprocessed fastas are missing.\n";
	}else{
		print_msg_to_filehandles(\@outFH, 
		"Using preprocessed fasta files created previously during the script run, filenames stored in $options->{'metafileFasta'}.");
	}

	foreach my $curQual ( @{$sortedQual} ) {
		foreach my $curLength ( @{$sortedLength} ) {	

        	        # location where analysis data are stored (data obtained running QIIME)
        	        my $analysisFolders = {};
	                my $analysisRoot = create_name($options->{'NZGLroot'},$options->{'name'},"analysis_q".$curQual."_l".$curLength."/",\@outFH);
                	$analysisFolders = create_folders_analysis($analysisRoot, \@outFH, $options->{'compareCategories'} eq 'True');

                	doTheQIIME(
				$analysisFolders,
				\@outFH, 
				$Fasta->{$curQual}{$curLength}, 
				$options,
				);

	                print_msg_to_filehandles(\@outFH, 
			"Analysis of fasta file $Fasta->{$curQual}{$curLength} by QIIME finished at: ".scalar(localtime));
		}
	}
	print_msg_to_filehandles(\@outFH, "QIIME analysis complete at: ".scalar(localtime));
}

print_msg_to_filehandles(\@outFH, "Script $0 complete at: ".scalar(localtime));
close LOG;






#####################
#                   
#    subroutines    
#                   
#####################


# output: fasta file ready for QIIME analysis, the file is stored in $newFolders->{'root'}:
# filename: $options->{name}."_readyForAnalysis_q".$curQual."_l".$curLength.".fa"
# take sequenced fastq files, 
# join pair end reads to single end (insert size < 2 x length of each read in pair) (FLASh)
# QC the single end reads, get rid of low quality and short reads ($curQual, $curLength) (SolexaQA++)
# convert fastq to fasta
# retain only non-chimeric reads (QIIME: identify_chimeric_seqs.py, filter_fasta.py)
# subsample all replicates to the same number of reads, if needed
sub create_fastafile {
	my (
		$newFolders, 
		$fhRef, 
		$curQual, 
		$curLength, 
		$options,
	) = @_;
		
	my $preprocessedFasta = $newFolders->{'root'}."readyForAnalysis_q".$curQual."_l".$curLength.".fa";
	if ( -e $preprocessedFasta ){
		system_call($fhRef, "rm -f $preprocessedFasta", "QIIMEstuff/rm -f $preprocessedFasta");
	}	

	open (IN, "<$options->{'metaCounts'}") or die ("couldn't open metaCounts $options->{'metaCounts'}: $!\n");
 	
	my @replicate = split /,/, $options->{'replicateString'};	

	while (<IN>) {
		next if $_ =~ m/^#/;
		chomp;
		my @meta = split;

		foreach my $curReplicate (sort @replicate) {

			next if $meta[$options->{'metaCountsReplicateField'}] != $curReplicate;

			my $required = $meta[$options->{'metaCountsIDfield'}]."_r".$curReplicate;

			my $workingFolder = ($options->{'NZGLroot'} . $meta[$options->{'metaCountsFolderField'}] . "/processed/");

			# $seq1, $seq2 -- names of fastq/fastq.gz 
			#                 files containing pair end reads from amplicon seq

			my @seq_array;
			my $unique_sampleID_string = "_".$meta[$options->{'metaCountsIDfield'}]."_";
			@seq_array = `ls $workingFolder | grep $unique_sampleID_string`;
			test_exit_status($?, "ls $workingFolder | grep $unique_sampleID_string");	
			if (scalar @seq_array != 2){ 
				my $num = scalar @seq_array;
				die "Folder $workingFolder contains unexpected number ($num) of files containing ID $meta[$options->{'metaCountsIDfield'}].\n" 
			}
			foreach my $s (@seq_array){ chomp($s); $s=$workingFolder.$s; }
			my $seq1 = $seq_array[0];
			my $seq2 = $seq_array[1];	

			#########################################################################
			#									
			# Define file names of files that are generated in this subroutine	
			#	 and used later on 						
			#									
			#########################################################################

			#solexa, flash file names 
			my $compact = ($required.".extendedFrags.fastq");
			my $inSeq = ($newFolders->{'all'}.$compact);
			my $trimmed = ($newFolders->{'toProcess'}.$compact.".trimmed");
			my $trimmedS = ($newFolders->{'toProcess'}.$compact.".trimmed.single");
			my $newTrimmed = ($newFolders->{'fQsequences'}.$required."_T.fastq");

			# temporary files, will be deleted in this subroutine
			# $modFa3 is added to the end of $preprocessedFasta file before removing   
			my $inFa = ($newFolders->{'root'} . $required . "_T.fa");
			my $modFa = ($newFolders->{'root'} . $required . "_TMod.fa");
			my $modFa2 = ($newFolders->{'root'} . $required . "_TchimFree.fa");		
			my $modFa3 = ($newFolders->{'root'} . $required . "_TMod2.fa");

			#chimFree file names
			my $chimaera = ($newFolders->{'chimFree'} . "chimeras.txt");
			my $nonChimaera	= ($newFolders->{'chimFree'} . "non_chimeras.txt");			
			my $chimaeraMod	= ($newFolders->{'chimFree'} . $required . "_chim.txt");
 			my $nonChimaeraMod = ($newFolders->{'chimFree'} . $required . "_nonChim.txt");

			#########################################################################
			#									
			# 1) Join pair end reads to single end (FLASh)
			# 2) Trim and sort the single end reads (SolexaQA++)
			# 3) Convert the filtered single end fastq file to fasta 
			#									
			# Input: $seq1, $seq2							
			# Output: $inFa								
			#########################################################################
			
			# 1)	
                        system_call($fhRef, 
				"flash $options->{'flashOptions'} $seq1 $seq2 -o $required -d $newFolders->{'all'}", 
				"QIIMEstuff/flash"
			);
			
			# 2)
			system_call($fhRef, 
				"SolexaQA++ dynamictrim -p $curQual $inSeq -d $newFolders->{toProcess}", 
				"QIIMEstuff/SolexaQA++ dynamictrim"
			);
			system_call($fhRef, 
				"SolexaQA++ lengthsort -l $curLength $trimmed -d $newFolders->{toProcess}", 
				"QIIMEstuff/SolexaQA++ lengthsort"
			);
			system_call($fhRef, 
				"mv $trimmedS $newTrimmed", 
				"QIIMEstuff/mv"
			);
			
			# 3)
			unless ( -e $newTrimmed ){die "No input fastq file $newTrimmed for conversion to fasta.\n"}

			my $inFastq = Bio::SeqIO->new(-format => "fastq", -file => $newTrimmed);
			my $outFasta = Bio::SeqIO->new(-format => "fasta", -file => ">$inFa");

			while (my $data = $inFastq->next_dataset()){
				my $seq = Bio::Seq::Quality->new(%$data);
				$outFasta->write_seq($seq);
			}

			print_msg_to_filehandles($fhRef, "--Fastq of $required converted to fasta at ".scalar(localtime));

			########################################################################################
			#									
			# 1) Convert fasta to QIIME-compatible format
			# 2) Retain only non-chimeric reads (QIIME: identify_chimeric_seqs.py, filter_fasta.py)
			#									
			# Input: $inFa								
			# Output: $modFa2						
			#									
			########################################################################################

			unless (-e $inFa ){ die "Fasta file converted from $newTrimmed does not exist.\n"}				
			convert_to_QIIME_compatible_fasta($inFa, $modFa, $meta[$options->{'metaCountsIDfield'}], $meta[$options->{'metaCountsFolderField'}]); 

			system_call($fhRef, 
				"identify_chimeric_seqs.py -i $modFa -m usearch61 -o $newFolders->{'chimFree'} --suppress_usearch61_ref", 
				"QIIMEstuff/identify_chimeric_seqs.py"
			);
			system_call($fhRef, 
				"filter_fasta.py -f $modFa -o $modFa2 -s $newFolders->{'chimFree'}/non_chimeras.txt", 
				"QIIMEstuff/filter_fasta.py"
			);
			system_call($fhRef, 
				"mv $chimaera $chimaeraMod", 
				"QIIMEstuff/mv $chimaera $chimaeraMod"
			);
			system_call($fhRef, 
				"mv $nonChimaera $nonChimaeraMod", 
				"QIIMEstuff/mv $nonChimaera $nonChimaeraMod"
			);	
			system_call($fhRef, 
				"rm -f $inFa $modFa", 
				"QIIMEstuff/rm -f $inFa $modFa"
			);
		}
		

		if ($options->{'subsample'} eq 'True'){

                        # subsample          
                        # input: $modFa2
       	                # output: $preprocessedFasta
			my @counts;
			print_msg_to_filehandles($fhRef, "$meta[$options->{'metaCountsIDfield'}] replicates read counts follow:");
			foreach my $curReplicate (sort @replicate){
				next if $meta[$options->{'metaCountsReplicateField'}] != $curReplicate;
				my $required = $meta[$options->{'metaCountsIDfield'}]."_r".$curReplicate;

				my $modFa2 = ($newFolders->{'root'} . $required . "_TchimFree.fa");
				my $c = `grep '^>' -c $modFa2`;
				chomp($c);
				push @counts, int($c);
			
				print_msg_to_filehandles($fhRef, "Replicate $curReplicate read count: $c");
			}
			my @sorted_counts = sort {$a <=> $b} @counts;
			my $minCount = $sorted_counts[0];
			print_msg_to_filehandles($fhRef, "Read count in the smallest replicate: $minCount");
		
			my $sampleSize = int ($minCount * $options->{'subRatio'});
			print_msg_to_filehandles($fhRef, "Read count subsampled from every replicate: $sampleSize");
			
			my $i = 0;
			foreach my $curReplicate (sort @replicate){
				next if $meta[$options->{'metaCountsReplicateField'}] != $curReplicate;

				my $required = $meta[$options->{'metaCountsIDfield'}]."_r".$curReplicate;
                        	my $modFa2 = ($newFolders->{'root'} . $required . "_TchimFree.fa");
	                        my $modFa3 = ($newFolders->{'root'} . $required . "_TMod2.fa");

				subsample_fasta($modFa2, $modFa3, $sampleSize, $counts[$i], $options->{'subsampleFunction'});
				system_call($fhRef, 
					"cat $modFa3 >> $preprocessedFasta", 
					"QIIMEstuff/ cat $modFa3 >> $preprocessedFasta"
				);
				system_call($fhRef, 
					"rm -f $modFa2 $modFa3",  
					"QIIMEstuff/rm -f $modFa2 $modFa3"
				);
				$i += 1;	
			}	
		}elsif($options->{'subsample'} eq 'False'){

			# take all fasta sequences for each replicate
			#
			# input: $modFa2	
			# output: $preprocessedFasta
			foreach my $curReplicate (sort @replicate){
				next if $meta[$options->{'metaCountsReplicateField'}] != $curReplicate;

				my $required = $meta[$options->{'metaCountsIDfield'}]."_r".$curReplicate;
				my $modFa2 = ($newFolders->{'root'} . $required . "_TchimFree.fa");

				system_call($fhRef, "cat $modFa2 >> $preprocessedFasta", "QIIMEstuff/cat $modFa2 >> $preprocessedFasta");
				system_call($fhRef, "rm -f $modFa2", "QIIMEstuff/rm -f $modFa2");
			}
		}else{ die "Option '--subsample' equals to $options->{'subsample'}. Should be either 'True' or 'False'." }
	}
	close IN;
	return $preprocessedFasta;
}

# run various QIIME scripts to get results of the analysis 
sub doTheQIIME {
	my (
		$newFolders, 
		$fhRef, 
		$inFa, 
		$options,
	) = @_;



	my $metafile = $options->{'metafile'};

	#########################################################################################
	#                                                                                       
	# define file names of files that are generated in this subroutine and used later on    
	#                                                                                       
	#########################################################################################


	my $biom	= $newFolders->{'otus'}."otu_table.biom";
	my $stats	= $newFolders->{'otus'}."otu_table_summary.txt";
	my $repSet	= $newFolders->{'otus'}."rep_set.tre";

    	# make OTUs #
	system_call($fhRef, 
		"pick_de_novo_otus.py -i $inFa -o $newFolders->{'otus'} $options->{'pickDeNovoOtusOptions'}", 
		"doTheQIIME/pick_de_novo_otus.py"
	);
	system_call($fhRef, 
		"biom summarize-table -i $biom -o $stats $options->{'biomSummarizeTableOptions'}", 
		"doTheQIIME/biom summarize-table"
	);   
	print_msg_to_filehandles($fhRef, "--OTUs picked at ".scalar(localtime));

        # convert biom file to json file format

        my ($f,$p) = fileparse($biom);
        my @d = split /\/|_analysis_/, $biom;
        my $out = $p.join("_", $d[scalar @d - 4], $d[scalar @d - 3]).".otu_table.json.biom";

        system_call($fhRef,
                "biom convert -i $biom -o $out --to-json --table-type 'OTU table' --header-key taxonomy",
                "doTheQIIME/biom convert"
        );
 
	# OTU Network #
	system_call($fhRef, 
		"make_otu_network.py -m $metafile -i $biom -o $newFolders->{'otus'}/OTU_Network $options->{'makeOtuNetworkOptions'}", 
		"doTheQIIME/make_otu_network.py"
	);
	print_msg_to_filehandles($fhRef, "--Network made at ".scalar(localtime));

	# Make Taxa Summary Charts #
	system_call($fhRef, 
		"summarize_taxa_through_plots.py -i $biom -o $newFolders->{'taxaSum'} -m $metafile $options->{'summarizeTaxaThroughPlotsOptions'}", 
		"doTheQIIME/summarize_taxa_through_plots.py"
	);
	print_msg_to_filehandles($fhRef, "--Taxa summaries made at ".scalar(localtime));


        # Convert biom file to json file format, for summarize_taxa_through_plots.py outputs #
	for (my $i = 2; $i <= 6; $i++){

		my $filePath = `ls $newFolders->{'taxaSum'}/*otu_table*_L$i.biom`;
		chomp($filePath);
		my $fileName = `basename ${filePath}`;
		chomp($fileName);

		my $p =  $newFolders->{'taxaSum'}."/".$fileName;
	        system_call($fhRef,
        	        "biom convert -i $p -o $newFolders->{'taxaSum'}/otu_table_L$i.json.biom --to-json --table-type 'OTU table' --header-key taxonomy",
                	"doTheQIIME/biom convert otu_table_L$i"
	        );
	}
	print_msg_to_filehandles($fhRef, "--Taxa summaries converted to json.biom at ".scalar(localtime));



        # OTU Heatmap #
##
##      valid for QIIME version <= 1.8
##
#       system_call($fhRef, 
#               "make_otu_heatmap_html.py -m $metafile -i $biom -o ".$newFolders->{'otus'}."/OTU_Heatmap $options->{'makeOtuHeatmapOptions'}", 
#               "doTheQIIME/make_otu_heatmap_html.py"
#       );      
#       print_msg_to_filehandles($fhRef, "--Heatmap made at ".scalar(localtime));



	# OTU Heatmaps based on taxa summary tables from summarize_taxa_through_plots.py #

#        for (my $i = 2; $i <= 6; $i++){
#                my $height = 2*$i;
#                system_call($fhRef,
#                        "make_otu_heatmap.py -m $metafile -i $newFolders->{'taxaSum'}/otu_table_L$i.json.biom -o $newFolders->{'otus'}/OTU_Heatmap_L$i.pdf $options->{'makeOtuHeatmapOptions'}",
#                        "doTheQIIME/make_otu_heatmap.py"
#                );
#
##                system_call($fhRef,
##                        "make_otu_heatmap.py -m $metafile -i $newFolders->{'taxaSum'}/otu_table_L$i.json.biom -o ".$newFolders->{'otus'}."/OTU_Heatmap_L$i.pdf --height $height $options->{'makeOtuHeatmapOptions'}",
##                        "doTheQIIME/make_otu_heatmap.py"
##                );
#
#
#        }
#        print_msg_to_filehandles($fhRef, "--Heatmaps made at ".scalar(localtime));


	# Beta rarefaction and plots #
	system_call($fhRef, 
		"beta_diversity_through_plots.py -i $biom -m $metafile -o $newFolders->{'beta'} -t $repSet $options->{'betaDiversityThroughPlotsOptions'}", 
		"doTheQIIME/beta_diversity_through_plots.py"
	); 
	print_msg_to_filehandles($fhRef, "--Beta rarefaction complete at ".scalar(localtime));

	# Jackknifed beta diversity #
	system_call($fhRef, 
		"jackknifed_beta_diversity.py -i $biom -t $repSet -m $metafile -o $newFolders->{'jack'} $options->{'jackknifedBetaDiversityOptions'}", 
		"doTheQIIME/jackknifed_beta_diversity.py"
	);
	print_msg_to_filehandles($fhRef, "--Jackknifed beta diversity complete at ".scalar(localtime));

	# Make Bootstrapped Tree #
	system_call($fhRef, 
		"make_bootstrapped_tree.py -m $newFolders->{'jack'}/unweighted_unifrac/upgma_cmp/master_tree.tre -s $newFolders->{'jack'}/unweighted_unifrac/upgma_cmp/jackknife_support.txt -o $newFolders->{'jack'}/unweighted_unifrac/upgma_cmp/jackknife_named_nodes.pdf", 	
		"doTheQIIME/make_bootstrapped_tree.py"
	);
	print_msg_to_filehandles($fhRef, "--Bootstrapped tree made at ".scalar(localtime));
 
	# Alpha rarefaction #	
	system_call($fhRef, 
		"alpha_rarefaction.py  -i $biom -m $metafile -o $newFolders->{'alpha'} -t $repSet $options->{'alphaRarefactionOptions'}", 
		"doTheQIIME/alpha_rarefaction.py"
	);
	print_msg_to_filehandles($fhRef, "--Alpha rarefaction complete at ".scalar(localtime));


#	system_call($fhRef,
#		"rm -f $biom",
#		"doTheQIIME/rm hdf5 $biom"
#	);

	# Compare categories #
	if ($options->{'compareCategories'} eq 'True' ){
		my @categories = split /,/, $options->{'catString'};
		foreach my $category (@categories){
			system_call($fhRef, 
				"compare_categories.py --method anosim -i $newFolders->{'beta'}/weighted_unifrac_dm.txt -c $category -m $metafile -o $newFolders->{'compare'} $options->{'compareCategoriesOptions'}", 
				"doTheQIIME/compare_categories.py"
			);
		}	
		print_msg_to_filehandles($fhRef, "--Anosim comparison complete at ".scalar(localtime));
	}
}


#create metafile joining headerString and metacounts file content

sub create_metafile{
	my ($metafileName, $metaCountsHeaderString, $metaCounts) = @_;

	open (META, ">$metafileName") or die ("create_metafile/ couldn't open metafile $metafileName: $!\n");
	open (IN, "<$metaCounts") or die ("create_metafile/ couldn't open metaCounts $metaCounts: $!\n");
	print META ('#', join("\t", split(/,/, $metaCountsHeaderString)), "\n");
	while (<IN>) {
		next if $_ =~ m/^#/;
		chomp;
		my @meta = split;
		print META (join("\t", @meta), "\n");
	}
	close IN;
	close META;
}


sub load_preprocessed_fasta_names{
	my ($metafileFasta, $sortedQualRef, $sortedLengthRef, $fhRef) = @_;

	my %Fasta = ();

	open (FH, "<$metafileFasta") or die "load_preprocessed_fasta_names/ couldn't open metafileFasta file $metafileFasta: $!.\n";
	my $i=0;
	while (my $line = <FH>){
		next if $line =~ m/^#/;
		chomp($line);
		my @F = split /\t/, $line;
		for (my $j=0; $j< scalar @F; $j++){
			$Fasta{$sortedQualRef->[$i]}{$sortedLengthRef->[$j]} = $F[$j];
		}
		$i+=1;
	}
	close FH;

	print_msg_to_filehandles($fhRef, "Load preprocessed fasta-file names from $metafileFasta.");
	return \%Fasta;
}


sub save_preprocessed_fasta_names{ 
	my ($metafileFasta, $sortedQualRef, $sortedLengthRef, $FastaRef, $fhRef) =@_;

	open (FH, ">$metafileFasta") or die "save_preprocessed_fasta_names/ couldn't open metafileFasta file $metafileFasta: $!.\n";

	for (my $i = 0; $i < scalar @{$sortedQualRef}; $i++){
		my $j=0;
		while ($j < scalar @{$sortedLengthRef} - 1){
			print FH $FastaRef->{$sortedQualRef->[$i]}{$sortedLengthRef->[$j]}."\t";
			$j+=1;	
		}	
		print FH $FastaRef->{$sortedQualRef->[$i]}{$sortedLengthRef->[$j]}."\n";
	}

	close FH;

	print_msg_to_filehandles($fhRef, "Save preprocessed fasta-file names in $metafileFasta."); 
}


# change fasta header so that QIIME can parse it
sub convert_to_QIIME_compatible_fasta{
	my ($inFa, $compatibleFa, $sampleIDmeta, $projectIDmeta) = @_;

	my $lineCount = 1;
	my $DNAref = Bio::SeqIO->new ( -format => 'Fasta', -file => $inFa );

	open (SEQ, ">$compatibleFa") or die ("couldn't open $compatibleFa: $!\n");
	while (my $DNAobj = $DNAref->next_seq()) {
		my $DNAseq      = $DNAobj->seq();
		my $DNAheader   = $DNAobj->display_id();
		my $sampleID    = ($sampleIDmeta."_".$lineCount);
		print SEQ (">$sampleID $DNAheader orig_bc=$projectIDmeta new_bc=$projectIDmeta bc_diffs=0\n$DNAseq\n");
		$lineCount++;
	}
	close SEQ;
}


sub subsample_fasta{
	my ($inFaFile, $outFaFile, $sampleSize, $totalSize, $subsampleFunction)=@_;

	if ($subsampleFunction eq 'new'){ 
		subsample_fasta_new($inFaFile, $outFaFile, $sampleSize, $totalSize);
	}elsif($subsampleFunction eq 'old'){
		subsample_fasta_old($inFaFile, $outFaFile, $sampleSize, $totalSize);
	}else{die "option '--subsampleVersion' should be either 'new' or 'old', but it is set to $subsampleFunction."}
}

# random subsample of fasta file, pick $limit sequences from $total, 
# iterate through all fasta sequences in the file, 
# in each iteration either pick or do not pick the sequence into a subsample and adjust 
# $limit_var and $total_var accordingly.
#
# $limit_var -- number of sequences that still need to be drown from the rest of the fasta file 
# $total_var -- number of fasta sequences before the file ends
# 
# at the end, subsampled fasta sequences are uniformly distributed 
# through the input file and only memory of the subsample size is used in the process.
sub subsample_fasta_new{
	my ($inFaFile, $outFaFile, $limit, $total) = @_;

	my $picked_sofar = 0;
	my $limit_var = $limit;
	my $total_var = $total;
	my $r;

	open (OUT, ">$outFaFile") or die ("couldn't open $outFaFile: $!\n");

	my $DNAref = Bio::SeqIO->new (-format => 'Fasta', -file => "$inFaFile");	
	while (my $DNAobj = $DNAref->next_seq()) {
		$r = rand($total_var); 
		if ($r < $limit_var){		
			print OUT ">".$DNAobj->display_id()."\n".$DNAobj->seq()."\n";
			$limit_var -= 1;
		}
		$total_var -= 1;
	}

	close OUT;
};


# random subsample of fasta file
# store first $limit sequences in memory
# iterate through the rest of the file
# occasionaly, with probability $limit/$total swap a sequence 
# at random position (uniformly distributed) with the newly read sequence
# works on fasta file where each sequence takes 
# exactly two lines (a header line and a sequence line)
sub subsample_fasta_old{
	my ($inFaFile, $outFaFile, $limit, $total) = @_;
	
	my @samples;
	my $n = 0;      # total number of sequences read
	my $i;          # index of current sequence

	open (IN, "<$inFaFile") or die ("couldn't open $inFaFile: $!\n");
	open (OUT, ">$outFaFile") or die ("couldn't open $outFaFile: $!\n");

	while (<IN>) {
		if (/^\>/) {
			$i              = int rand ++$n;                # Select a random sequence from 0 to $n
			$samples[$n-1]  = $samples[$i] if $n <= $limit; # Save all samples until we've accumulated $k of them:
			$samples[$i]    = [] if $i < $limit;            # Only actually store the new sequence if it's one of the $k first ones:
		}
		push @{ $samples[$i] }, $_ if $i < $limit;
	}

	print OUT @$_ for @samples;
	close OUT;
	close IN;
};

# create folder structure for preprocess outputs storage
# at a location provided in $preprocessRoot
# if a folder exists, remove it with all the content 
# and create again
sub create_folders_preprocess{
        my ($preprocessRoot, $fhRef)=@_;

        # $newFolders->{toProcess}: keeps SolexaQA++ outputs stored 
        # $newFolders->{all}: keeps FLASh output stored
        # $newFolders->{fQsequences}: SolexaQA++ trimmed,sorted output is moved here
        # $newFolders->{chimFree}: non-chimeric reads identified by QIIME 
        # $newFolders->{root}: root

        my %newFolders = (
                'toProcess' => $preprocessRoot."toProcess/",
                'all' => $preprocessRoot."all/",
                'fQsequences' => $preprocessRoot."fQsequences/",
                'chimFree' => $preprocessRoot."chimaeraFree/"
        );

	foreach my $key (sort keys %newFolders){
                if ( -e $newFolders{$key} ){
                        system_call($fhRef, "rm -rf $newFolders{$key}", "create_folders_preprocess/rm");
                }
                system_call($fhRef, "mkdir -p $newFolders{$key}", "create_folders_preprocess/mkdir");
        }

	$newFolders{'root'} = $preprocessRoot;
        return \%newFolders;
}

# create folder structure for analysis outputs storage
# at a location provided in $analysisRoot
# if a folder exists, remove it with all the content 
# and create again
sub create_folders_analysis{
        my ($analysisRoot, $fhRef, $cmp) =@_;

        my %newFolders = (
                'otus'          => $analysisRoot."otus/",
                'taxaSum'	=> $analysisRoot."wf_taxa_summary/",
                'alpha'         => $analysisRoot."wf_alpha/",
               	'beta'          => $analysisRoot."wf_beta/",
                'jack'          => $analysisRoot."wf_jack/",
       	);
	
	if ($cmp){
		$newFolders{'compare'} = $analysisRoot."compare/";
	}

        foreach my $key (sort keys %newFolders){
                if ( -e $newFolders{$key} ){
                        system_call($fhRef, "rm -rf $newFolders{$key}", "create_folders_analysis/rm");
                }
                system_call($fhRef, "mkdir -p $newFolders{$key}", "create_folders_analysis/mkdir");
        }

        $newFolders{'root'} = $analysisRoot;
        return \%newFolders;
}


###########################
#
# technical subroutines:
#	system calls,
#	print input options
###########################

# used for uniform handling of file/folder names
# when $option->{'name'} is (not) provided
sub create_name{
	my ($start,$name,$end,$fhRef)=@_;

	my $result = '';

	if ($name eq ''){
		$result = $start.$end;
	}else{
		$result = join('',$start,$name,"/",$end);
		system_call($fhRef, "mkdir -p $start$name", "create_name:mkdir");
	}
	return $result;
}


sub test_exit_status{
	my ($exitStatus, $err_msg) = @_;

	if ($exitStatus != 0){
		die "ERROR: exit status $exitStatus\nERROR: $err_msg.\n";
	}
}


sub print_msg_to_filehandles{
	my ($fhRef, $msg) = @_;

	foreach my $fh (@{$fhRef}){
		print {$fh} $msg."\n\n";
	}
}


# run system call and die at non-zero exit status 
sub system_call{
	my ($fhRef, $cmd, $err_msg) = @_;

        print_msg_to_filehandles($fhRef, $cmd);
        system "$cmd";
        test_exit_status($?, $err_msg);
}


##########################################
#
# misc:
#  mostly one time usage, 
#  encapsulate code for better readibility
#  of the main part
#
##########################################


# used in main
# input: reference to array of filehandles, reference to hash of all options
# output: list of variable names and corresponding values 
#	  written to all filehandles in that array
sub print_options_setting{
	my ($fhRef, $options) = @_;

	foreach my $fh (@{$fhRef}){
		print {$fh} "Script $0 runs with the following option setting:\n";	
		foreach my $option (sort keys %{$options}){
			if (defined $options->{$option}){
				if ( $options->{$option} eq ''){
					print {$fh} "\t".$option." = \'\'\n"; 
				}else{
					print {$fh} "\t".$option." = ".$options->{$option}."\n" ;
				}
			}else{
				print {$fh} "\t".$option." = \'\'\n";
			}
		}
	}
}

# used in main before GetOptions
# to set default values
sub set_default_option_values{
	my $options = {};

	$options->{'help'}=0;
	$options->{'stepString'}="preprocess,analysis";
	$options->{'qualString'}='0.003';
	$options->{'lengthString'}='250';
	$options->{'replicateString'}='1';
	$options->{'flashOptions'}='-M 200';
	$options->{'subsample'}='True';
	$options->{'subRatio'}=0.2;
	$options->{'subsampleFunction'}='new';
	$options->{'pickDeNovoOtusOptions'}='-f --parallel';
	$options->{'biomSummarizeTableOptions'}='';
	$options->{'makeOtuHeatmapOptions'}='--suppress_column_clustering';
	$options->{'makeOtuNetworkOptions'}='';
	$options->{'summarizeTaxaThroughPlotsOptions'}='-f -s';
	$options->{'betaDiversityThroughPlotsOptions'}='-e 110 -f --parallel';
	$options->{'jackknifedBetaDiversityOptions'}='-e 110 -f --parallel';
	$options->{'alphaRarefactionOptions'}='-f --parallel';
	$options->{'compareCategories'}='False';
	$options->{'compareCategoriesOptions'}='--method anosim';
	$options->{'catString'}='replicate';

# other prev used settings:
# $options->{'flashOptions'} = "-r 150";
# $options->{'catString'} = "replicate,treatment,plot,plotTreatment";

	return $options;
}


sub set_values{
        my ($options) = @_;

        my @step = split(/\,/, $options->{'stepString'});
        my $preprocess = 0;
        my $analysis = 0;
        foreach my $s (@step){
                if ($s eq 'preprocess'){$preprocess = 1}
                if ($s eq 'analysis'){$analysis = 1}
        }

	my @length = split(/,/, $options->{'lengthString'});
        my @sortedLength = sort {$a <=> $b} @length;
        my @qual = split(/,/, $options->{'qualString'});
        my @sortedQual = sort {$a <=> $b} @qual;

        return ($preprocess, $analysis, \@sortedQual, \@sortedLength);
}


# check option setting provided by user and die 
# if there is not enough information provided 
# or there is conflict in given options (metaCounts 
# and metafile set at the same time, for example)
sub check_options_setting{ 
        my ($fhRef, $options) = @_;

        my ($preprocess, $analysis, @whatever) = set_values($options);

        unless ( $preprocess || $analysis ) {
		print_msg_to_filehandles($fhRef, 
			"ERROR: --step option is $options->{stepString}. Not defined correctly, pick one of the following 'preprocess'/'analysis'/'preprocess,analysis'.");
		die;
	}

        # check on existence of necessary options to run preprocess
        # check on conflicts in provided options for preprocess and analysis
        if ( $preprocess ){
                unless ( defined($options->{'metaCounts'}) ){
			print_msg_to_filehandles($fhRef, 
				"ERROR: --metaCounts option has to be defined for preprocessing step.");
			die;
		}
                unless ( defined($options->{'metaCountsIDfield'}) ){
                        print_msg_to_filehandles($fhRef,
				"ERROR: --metaCountsIDfield option has to be defined for preprocessing step.");
			die;
		}
                unless ( defined($options->{'metaCountsReplicateField'}) ){
                        print_msg_to_filehandles($fhRef,
                                "ERROR: --metaCountsReplicateField option has to be defined for preprocessing step.");
                        die;
                }
                unless ( defined($options->{'metaCountsFolderField'}) ){
                        print_msg_to_filehandles($fhRef,
                                "ERROR: --metaCountsFolderField option has to be defined for preprocessing step.");
                        die;
                }
                unless ( defined($options->{'metaCountsHeaderString'}) ){
                        print_msg_to_filehandles($fhRef,
				"ERROR: --metaCountsHeader option has to be defined for preprocessing step.");
			die;
		}
                if ( defined($options->{'metafile'}) ){
                        print_msg_to_filehandles($fhRef,
				"ERROR: either perprocessing --step should be done or --metafile $options->{'metafile'} provided.");
			die;
		}
                if ( defined($options->{'metafileFasta'}) ){
                        print_msg_to_filehandles($fhRef,
				"ERROR: either preprocessing --step should be done or --fastaFiles $options->{'metafileFasta'} provided.");
			die;
		}
		if ( ($options->{'subsample'} ne 'True') and ($options->{'subsample'} ne 'False') ){
			print_msg_to_filehandles($fhRef,
				"ERROR: option '--subsample' equals to $options->{'subsample'}. Should be either 'True' or 'False'.");
			 die;
		}
       	}

        # check on existence of necessary options to run analysis,
        # when preprocessing is skipped
        if ( !$preprocess && $analysis ){
                unless ( defined($options->{'metafile'}) ){
                        print_msg_to_filehandles($fhRef,
				"ERROR: --metafile option has to be defined, if only analysis --step should be done.");
			die;
		}
                unless ( defined($options->{'metafileFasta'}) ){
                        print_msg_to_filehandles($fhRef,
				"ERROR: --fastaFiles option has to be defined if only analysis --step should be done.");
			die;
		}
       	}

	if ( $analysis ){
		if ( ( $options->{'compareCategories'} eq 'True' ) && ( $options->{'catString'} eq '' ) ){
			die "ERROR: --category option has to be set if categories should be compared."
		}	

	}
}


__END__


=head1 NAME

QIIMEanalysis.pl - The script processes pair-end metagenomic samples. 

=head1 SYNOPSIS 

perl QIIMEanalysis.pl --help 

perl QIIMEanalysis.pl 
        --name S<test> \
        --projectFolder F<projectFolder> \
        --metaCounts F<metaCounts> \
        --metaCountsIDfield 0 \
        --metaCountsReplicateField 4 \
        --metacountsFolderField 3 \
        --metaCountsHeader S<SampleID,BarcodeSequence,LinkerPrimerSequence,folder,replicate,MGS> 

--minimal setting to run both steps (preprocessing and analysis) with default values for testing on a small subsample

=head1 OPTIONS

=over 4

=item B<--help>

Print this help page.


=item B<--projectFolder>

Full path to a folder where all output 
data are stored.

Mandatory.


=item B<--name> 

Prefix of folder/file names created in 
projectFolder during run of the script.

Default value: ''.


=item B<--step>

Possible values: 'preprocess,analysis' or 'preprocess' or 'analysis' 	

Specify which steps of the script run. 


=item B<--quality> 

Comma separated list of quality levels at which 
the QIIME analysis proceeds (parameter is passed 
to SolexaQA++ dynamictrim)

Default value: '0.003'.

Full run setting usually: '0.01,0.05'


=item B<--length>

Comma separated list of minimum lengths (?) 
used for length filtering (parameter is passed 
to SolexaQA++ lengthsort)	

Default value: '250'.

Full run setting usually: '250,400'


=item B<--replicate> 

Comma separated list of replicate names.

Default value: '1'.


=item B<--metaCounts>

Full path to a file containing tab separated table 
with its columns corresponding to '--metaCountsHeader'.

Recently used columns are: SampleID, BarcodeSequence, LinkerPrimerSequence, Folder, Replicate, MGS

Lines starting with '#' are considered to be comments and ignored.

Mandatory in preprocess step.


=item B<--metaCountsIDfield>

0-base number of a column where SampleID is stored.

Mandatory in preprocess step.


=item B<--metaCountsReplicateField>

0-base number of a column where replicate ID is stored.

Mandatory in preprocess step.


=item B<--metaCountsFolderField>

0-base number of a column where folder name is stored.

Mandatory in preprocess step.


=item B<--metacountsHeader>

Comma separated string of column names in the order corresponding to --metaCounts file.
			
Mandatory in preprocess step.


=item B<--flashOptions>

String that is passed to FLASh script.
Any combination of options for the script except -o, -d and input sequences, these are hard-wired.

Default value: '-M 200'.


=item B<--subsample>

Possible values 'True'/'False', all replicates treated the same way will be subsampled 
to the same number of reads, which is equal to (subsampleRatio * number of reads in the smallest replicate). 
Otherwise all reads from every replicate go into the analysis step.

Default value: 'True'.

=item B<--subsampleRatio> 

Ratio at which the smallest replicate is subsampled.

Default value: 0.2

Full run setting usually: 0.95 (if --subsample eq 'True')


=item B<--subsampleVersion>

Two subsampling algorithms implemented, 'old' is historically the first one, 
'new' is light-weighted in terms of memory usage and uniformly spread 
through the whole sample.

Default value: 'new'.


=item B<--metafile>

Tab separated file containing a header starting with '#' and followed by 
column names specified in metaCountsHeader. Then non-commented lines from 
metaCounts follow. 

Mandatory in runs that do 'analysis' step only. Otherwise has to be empty 
string and the file will be generated in the preprocess step.

Default value: ''.


=item B<--fastaFiles>

Tab separated file containing full file names of fasta files (one per each pair [qual,length]).

Mandatory in runs that do 'analysis' step only. Otherwise has to be empty 
string and the file will be generated in the preprocess step.

Default value: ''.


=item B<--pickDeNovoOtusOptions> 

String that is passed to pick_de_novo_otus.py script from QIIME. 
Any combination of options for the script except -i and -o, these are hard-wired.

Default value: '-f --parallel'.


=item B<--biomSummarizeTableOptions>

String that is passed to 'biom summarize-table' script from biom-format.
Any combination of options for the script except -i and -o, these are hard-wired.

Default value: ''.


=item B<--makeOtuHeatmapOptions>

String that is passed to 'make_otu_heatmap.py' script from QIIME.
Any combination of options for the script except -m, -i, and -o, these are hard-wired. 

Default value: '--suppress_column_clustering'.


=item B<--makeOtuNetworkOptions>

String that is passed to 'make_otu_network.py' script from QIIME.
Any combination of options for the script except -m, -i, and -o, these are hard-wired. 

Default value: ''.


=item B<--summarizeTaxaThroughPlotsOptions>

String that is passed to 'summarize_taxa_through_plots.py' script from QIIME.
Any combination of options for the script -m, -i, and -o, these are hard-wired. 

Default value: '-f -s'.


=item B<--betaDiversityThroughPlotsOptions>

String that is passed to 'beta_diversity_through_plots.py' script from QIIME.
Any combination of options for the script except -m, -i, -o, and -t, these are hard-wired.

Default value: '-e 110 -f --parallel'.


=item B<--jackknifedBetaDiversityOptions>

String that is passed to 'jackknifed_beta_diversity.py' script from QIIME.
Any combination of options for the script except -m, -i, -o, and -t, these are hard-wired.

Default value: '-e 110 -f --parallel'.


=item B<--alphaRarefactionOptions> 

String that is passed to 'alpha_rarefaction.py' script from QIIME.
Any combination of options for the script except -m, -i, -o, and -t, these are hard-wired. 

Default value: '-f --parallel'.


=item B<--compareCategiries> 

Possible values 'True'/'False'. If 'True', script 'compare_categories.py' from QIIME runs separately for each category in --catString.

Default value: 'False'.


=item B<--compareCategoriesOptions> 

String that is passed to 'compare_categories.py' script from QIIME.
Any combination of options for the script except -m, -i, -o, and -c these are hard-wired.

Default value: '--method anosim'.
#TODO: not tested yet for any other method.


=item B<--catString>

Comma separated list of categories for which 'compare_categories.py' script from QIIME runs, for each category separately. 

Mandatory if --compareCategories is 'True'.

Default value: ''.


=back


=head1 DETAILS

The program is used to process amplicon pair-end sequences 
to analyse metagenomic samples. The program assumes, that fragment size 
is less than two times read size, i.e. read ends from a pair 
of corresponding forward and reverse reads overlap and 
it makes sence to use FLASh to join the reads together.
 

Dependencies:

	QIIME suite -- both steps
	biom-format -- analysis step 
	FLASh -- preprocessing step
	SolexaQA++ -- preprocessing step
	usearch61 -- in $PATH for preprocessing step

The program's default values are set for test run (small sample, only high qual reads, just to see that everything works), 
if the values differs from settings that are usually used in full run, these are specified in 'Full run setting usually:'. 

=cut


