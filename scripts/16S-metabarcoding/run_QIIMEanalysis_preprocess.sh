#!/bin/bash

projectFolder="/active/nzgl02031"
name="wholeDataset"
metaCounts="metaCounts.txt"


SCRIPTS=$(pwd)

echo "QIIME: $QIIMEdir"
echo "name: ${name}"
echo "metafile: ${metafile}"
echo "fastaFiles: ${fastaFiles}"

${SCRIPTS}/QIIMEanalysis.pl \
        --name ${name} \
        --projectFolder ${projectFolder} \
 	--step 'preprocess' \
 	--quality '0.01' \
 	--length '250' \
        --replicate '1' \
        --metaCounts ${SCRIPTS}/${metaCounts} \
        --metaCountsIDfield 0 \
        --metaCountsReplicateField 4 \
        --metacountsFolderField 3 \
        --metaCountsHeader SampleID,BarcodeSequence,LinkerPrimerSequence,folder,replicate,MGS \
	--flashOptions '-M 200' \
	--subsample 'False'
	
	
echo "QIIMEanalysis.pl exit status: $?"