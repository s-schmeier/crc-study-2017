#!/usr/bin/env python
"""

Clustering heatmap from tabulated data.

Linkage methods:
http://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html#scipy.cluster.hierarchy.linkage

Metric:
http://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.pdist.html?highlight=distance.pdist#scipy.spatial.distance.pdist

VERSION HISTORY
0.5.1  2016/09/15    Added option for changinf dendrogram color.
                     Reorganised arguments into groups
                     Added option to remove colorbar
0.5.0  2016/02/09    mpl is deprecated. use matplotlib.colors instead.
                     Added solarized cmap.
0.4.1  2014/09/17    Enable rotation for column labels (not perfect yet)
                     Enabled gridlines
                     Cleaned code base
0.4    2014/08/19    Enabled all kinds of distance-metrics from scipy.spatial.distance.pdist
0.3.2  2014/08/04    Possiblilty to remove dendrograms; fixed the header bug
0.3.1  2014/06/19    Added legend title option.
0.3    2014/06/14    Tight bbox option to make figures fit better
0.2.5  2014/06/04    Changed default mpl_colors
0.2.4  2013/10/31    Possible horizontal column labels.
0.2.3  2013/09/25    Make all pylab colormaps available.
0.2.2  2013/09/20    Make colorbar min/max adjustable.
0.2.1  2013/09/16    Adjusted the row labels to center position, make label-font
                     and window size adjustable.
0.2    2013/09/12    Adjusted image size and font based on number of rows and col.
0.1.2  2013/03/08    Changed the color scaling so that the max and min are
                     represented better on the scale.
0.1.1  2013/03/07    Bug fixes.
0.1    2013/03/07    Initial version.

"""
__version__='0.5.0'
__date__='2016/02/09'
__email__='s.schmeier@gmail.com'
__author__='Sebastian Schmeier'
import sys, os, argparse, csv, time, string

import matplotlib
matplotlib.use('Agg')

import scipy
import scipy.cluster.hierarchy as sch
import scipy.spatial.distance as dist
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import colorbar as mpl_colorbar
from matplotlib import colors as mpl_colors

def read_matrix(oF, delimiter, row_header, column_header):
    """ """
    aMatrix = []
    aRow = []
    aCol = []
    oR = csv.reader(oF, delimiter=delimiter)
    aLine = oR.next() # first line
    if column_header:
        if row_header:
            aCol = aLine[1:]
        else:
            aCol = aLine
    else:
        if row_header: aRow.append(aLine.pop(0))
        aMatrix.append([float(s) for s in aLine])

    for aLine in oR:
        if row_header: aRow.append(aLine.pop(0))
        aMatrix.append([float(s) for s in aLine])

    return np.array(aMatrix), aRow, aCol

def red_black_green():
    """
    Create Custom Color Gradient.
    """
    cdict = {'red':   ((0.0, 0.0, 0.0),
                       (0.5, 0.0, 0.1),
                       (1.0, 1.0, 1.0)),

             'blue': ((0.0, 0.0, 0.0),
                       (1.0, 0.0, 0.0)),

             'green':  ((0.0, 0.0, 1.0),
                       (0.5, 0.1, 0.0),
                       (1.0, 0.0, 0.0))
            }

    cmap = mpl_colors.LinearSegmentedColormap('my_colormap',cdict,256)
    return cmap

def solarized():
    """
    Create solarized cmap
    """
    colorpool = ['#b58900', '#cb4b16', '#dc322f', '#d33682', '#6c71c4', '#268bd2', '#2aa198', '#859900']
    cmap = mpl_colors.LinearSegmentedColormap.from_list('solarized', colorpool)
    return cmap

def hmap(x,
         row_header,
         column_header,
         row_method=None,
         column_method=None,
         row_metric=None,
         column_metric=None,
         color_gradient='jet',
         filename='heatmap.png',
         colorbar="linear",
         no_colorbar=False,
         no_row_labels=False,
         no_col_labels=False,
         iRowFont = None,
         iColFont = None,
         fWidth = None,
         fHeight = None,
         colormax=None,
         colormin=None,
         col_labels_degree = 0,
         bNoTight=False,
         sLegendTitle = '',
         no_row_dendrogram=False,
         no_col_dendrogram=False,
         dcolor='#515151',
         gridAlpha=.0
         ):
    """
    Heavily adapted from: Nathan Salomonis's hierarchical_clustering.py
    http://code.activestate.com/recipes/578175-hierarchical-clustering-heatmap-python/

    This below code is based in large part on the protype methods:
    http://old.nabble.com/How-to-plot-heatmap-with-matplotlib--td32534593.html
    http://stackoverflow.com/questions/7664826/how-to-get-flat-clustering-corresponding-to-color-clusters-in-the-dendrogram-cre

    x is an m by n ndarray, m observations (rows), n genes (columns)
    """
    n = len(x[0]); m = len(x)

    ### Define the color gradient to use based on the provided name
    if color_gradient == 'red_black_green':
        cmap = red_black_green()
    elif color_gradient == 'solarized':
        cmap = solarized()
    else:
        cmap = eval("plt.cm.%s" %(color_gradient))

    if colorbar=="mirror":
        if colormax:
            vmax = colormax
        else:
            ### Scale the max and min colors so that 0 is white/black in the middle
            vmax = max([x.max(),abs(x.min())])
        vmin = vmax*-1

    elif colorbar=="linear":
        if colormax != None:
            vmax = colormax
        else:
            vmax = x.max()
        if colormin != None:
            vmin = colormin
        else:
            vmin = x.min()

    norm = mpl_colors.Normalize(vmin, vmax)

    ####### THE SCALING OF THE IMAGE SIZE NEEDS IMPROVEMENT #########
    ### FIGSIZE
    if not fWidth:  ### SET AUTOMATICALLY
        window_width = 12
        ### scale image size
        if len(column_header) > 4*len(row_header):
            window_width = 24
    else:
        window_width = fWidth

    if not fHeight:  ### SET AUTOMATICALLY
        window_hight = 12
        ### scale image size
        if len(row_header) > 4*len(column_header):
            window_hight = 24
    else:
        window_hight = fHeight

    fig = plt.figure(figsize=(window_width,window_hight)) ### could use m,n to scale here
    color_bar_w = 0

    ### calculate positions for all elements

    ### ax1, placement of dendrogram 1, on the left of the heatmap
    ### if row_method != None: w1 =
    [ax1_x, ax1_y, ax1_w, ax1_h] = [0.05,0.22,0.2,0.6]   ### The second value controls the position of the matrix relative to the bottom of the view

    ### axr, placement of row side colorbar
    [axr_x, axr_y, axr_w, axr_h] = [0.31,0.1,color_bar_w,0.6] ### second to last controls the width of the side color bar - 0.015 when showing
    axr_x = ax1_x + ax1_w
    axr_y = ax1_y
    axr_h = ax1_h
    width_between_axr_axm = 0.002 ### space between side dendorgram and heatmap

    ### axc, placement of column side colorbar
    [axc_x, axc_y, axc_w, axc_h] = [0.4,0.63,0.5,color_bar_w] ### last one controls the hight of the top color bar - 0.015 when showing
    axc_x = axr_x + axr_w + width_between_axr_axm
    axc_y = ax1_y + ax1_h
    height_between_axc_ax2 = 0.001 ### space between top dendogram and heatmap

    ### axm, placement of heatmap for the data matrix
    [axm_x, axm_y, axm_w, axm_h] = [0.4,0.9,2.5,0.5]
    axm_x = axr_x + axr_w + width_between_axr_axm
    axm_y = ax1_y
    axm_h = ax1_h
    axm_w = axc_w

    ### ax2, placement of dendrogram 2, on the top of the heatmap
    [ax2_x, ax2_y, ax2_w, ax2_h] = [0.3,0.72,0.6,0.08] ### last one controls hight of the dendrogram
    ax2_x = axr_x + axr_w + width_between_axr_axm
    ax2_y = ax1_y + ax1_h + axc_h + height_between_axc_ax2
    ax2_w = axc_w

    ### axcb - placement of the color legend
    #[axcb_x, axcb_y, axcb_w, axcb_h] = [0.07,0.88,0.18,0.05]
    [axcb_x, axcb_y, axcb_w, axcb_h] = [0.06,0.87,0.18,0.02] # last one controls legend heights

    ### Compute and plot top dendrogram
    if column_method != None:
        d2 = dist.pdist(x.T, metric=column_metric)
        D2 = dist.squareform(d2)
        ax2 = fig.add_axes([ax2_x, ax2_y, ax2_w, ax2_h], frame_on=False)
        Y2 = sch.linkage(D2, method=column_method, metric=column_metric)
        Z2 = sch.dendrogram(Y2,
                            no_plot=no_col_dendrogram,
                            link_color_func=lambda x: dcolor) # make all links same color
        ## no colored sub trees
        #Z2 = sch.dendrogram(Y2, no_plot=no_col_dendrogram, color_threshold=0)
        ind2 = sch.fcluster(Y2,0.7*max(Y2[:,2]),'distance') ### This is the default behavior of dendrogram
        ax2.set_xticks([]) ### Hides ticks
        ax2.set_yticks([])

    else:
        ind2 = ['NA']*len(column_header) ### Used for exporting the flat cluster data

    # Compute and plot left dendrogram.
    if row_method != None:
        d1 = dist.pdist(x, metric=row_metric) # change metric here!
        D1 = dist.squareform(d1)  # full matrix
        ax1 = fig.add_axes([ax1_x, ax1_y, ax1_w, ax1_h], frame_on=False)
        Y1 = sch.linkage(D1, method=row_method, metric=row_metric)
        Z1 = sch.dendrogram(Y1,
                            orientation='left', # orientatation for dendrogram
                            no_plot=no_row_dendrogram,
                            link_color_func=lambda x: dcolor) # make all links same color
                            
        ind1 = sch.fcluster(Y1,0.7*max(Y1[:,2]),'distance') ### This is the default behavior of dendrogram
        ax1.set_xticks([]) ### Hides ticks
        ax1.set_yticks([])
    else:
        ind1 = ['NA']*len(row_header) ### Used for exporting the flat cluster data

    # Plot distance matrix.
    axm = fig.add_axes([axm_x, axm_y, axm_w, axm_h])  # axes for the data matrix
    xt = x
    if column_method != None:
        idx2 = Z2['leaves'] ### apply the clustering for the array-dendrograms to the actual matrix data
        xt = xt[:,idx2]
        ##ind2 = ind2[:,idx2] ### reorder the flat cluster to match the order of the leaves the dendrogram
        ## SEB
        ind2 = ind2[idx2]
    if row_method != None:
        idx1 = Z1['leaves'] ### apply the clustering for the gene-dendrograms to the actual matrix data
        xt = xt[idx1,:]   # xt is transformed x
        #ind1 = ind1[idx1,:] ### reorder the flat cluster to match the order of the leaves the dendrogram
        ## SEB
        ind1 = ind1[idx1]

    ### THIS PLOTS THE MATRIX
    ### taken from http://stackoverflow.com/questions/2982929/plotting-results-of-hierarchical-clustering-ontop-of-a-matrix-of-data-in-python/3011894#3011894
    ### norm=norm added to scale coloring of expression with zero = white or black
    im = axm.matshow(xt, aspect='auto', origin='lower', cmap=cmap, norm=norm)

    ## GRID ------------------------------------
    aXticks = np.arange(0,xt.shape[1])-0.5
    aYticks = np.arange(0,xt.shape[0])-0.5
    plt.xticks(aXticks, [])
    plt.yticks(aYticks, [])
    if gridAlpha == 0:
        plt.xticks([], [])
        plt.yticks([], [])
    axm.grid(ls='-', alpha=gridAlpha, color="white")
    plt.tick_params(length=0) # no ticks
    ##------------------------------------------

    ### Set row label font size based on matrix elements
    if not iRowFont:
        if len(row_header)>300 or len(column_header)>300:
            iRowFont = 2
        elif len(row_header)>220 or len(column_header)>220:
            iRowFont = 4
        elif len(row_header)>50 or len(column_header)>50:
            iRowFont = 5
        else:
            iRowFont = 8

    ### Set column label font size based on matrix elements
    if not iColFont:
        if len(row_header)>300 or len(column_header)>300:
            iColFont = 2
        elif len(row_header)>220 or len(column_header)>220:
            iColFont = 4
        elif len(row_header)>50 or len(column_header)>50:
            iColFont = 5
        else:
            iColFont = 8

    ### ROW LABELS
    if not no_row_labels:
        for i in range(x.shape[0]):
            if row_method != None:
                axm.text(x.shape[1]-0.5, i, '  '+row_header[idx1[i]], verticalalignment="center", fontsize=iRowFont)
            else:
                axm.text(x.shape[1]-0.5, i, '  '+row_header[i], verticalalignment="center", fontsize=iRowFont) ### When not clustering rows

    ### FIXME: Column label rotation is not perfect yet. 0, 90, 270, 180, work but others are not correctly placed.
    ### COLUMN LABELS
    if not no_col_labels:
        for i in range(x.shape[1]):
            if column_method != None:
                axm.text(i, -0.9, ' '+column_header[idx2[i]], rotation=col_labels_degree, verticalalignment="top", horizontalalignment="center", fontsize=iColFont) ### rotation in degrees
            else: ### When not clustering columns
                axm.text(i, -0.9, ' '+column_header[i], rotation=col_labels_degree, verticalalignment="top", horizontalalignment="center", fontsize=iColFont)

    # Plot color legend, position:
    if no_colorbar==False:
        axcb = fig.add_axes([axcb_x, axcb_y, axcb_w, axcb_h], frame_on=False)  # axes for
        cb = mpl_colorbar.ColorbarBase(axcb, cmap=cmap, norm=norm, orientation='horizontal')
        axcb.set_title(sLegendTitle)
        # Turn ticklabels
        for label in axcb.get_xaxis().get_ticklabels():
            label.set_rotation(270)

    if bNoTight:
        plt.savefig(filename)
    else:
        plt.savefig(filename, bbox_inches='tight')

    return

def parse_cmdline():

    # parse cmd-line -----------------------------------------------------------
    sDescription = 'Read delimited file and create clustering heatmap.'
    sVersion='version %s, date %s' %(__version__,__date__)
    sEpilog = 'Copyright %s (%s)' %(__author__, __email__)

    parser = argparse.ArgumentParser(description=sDescription,
                                      version=sVersion,
                                      epilog=sEpilog)
    parser.add_argument('sFile',
                         metavar='FILE',
                         help='Delimited file. [if set to "-" or "stdin" reads from standard in]')
    parser.add_argument('sFileOut',
                         metavar='OUTFILE',
                         help='Filename for the figure. Fileending determines file-type, e.g. .pdf, .eps, .png, .tif')
    parser.add_argument('-d', '--delimiter',
                         metavar='STRING',
                         dest='sDEL',
                         default='\t',
                         help='Delimiter used in file.  [default: "tab"]')
    parser.add_argument('--clabels',
                         dest='cHead',
                         action='store_true',
                         default=False,
                         help='Column labels in file. First row is assumed to contain column labels. [default: False]')
    parser.add_argument('--rlabels',
                         dest='rHead',
                         action='store_true',
                         default=False,
                         help='Row labels in file. First column is assumed to contain row-labels [default: False]')
    parser.add_argument('--pheight',
                         metavar="FLOAT",
                         type=float,
                         dest='fHeight',
                         default=None,
                         help='Set the picture-height in inches. [default: Set automatically]')
    parser.add_argument('--pwidth',
                         metavar="FLOAT",
                         type=float,
                         dest='fWidth',
                         default=None,
                         help='Set the picture-width in inches. [default: Set automatically]')
    parser.add_argument('--no_tight',
                         dest='bNoTight',
                         action='store_true',
                         default=False,
                         help='Turn tight layout off. [default: use tight layout]')
    
    groupV = parser.add_argument_group('Visual', ' Visual specific arguments:')
    groupV.add_argument('--color',
                         metavar="STRING",
                         dest='sColor',
                         default='RdYlBu_r',
                         help='Custom colormap: "red_black_green", "solarized" or any other color map from pylab.cm, e.g. Spectral_r, jet, rainbow, OrRd, YlGnBu, bwr, Blues, Reds etc.  [default: RdYlBu_r]. Examples of possible colormaps here: http://wiki.scipy.org/Cookbook/Matplotlib/Show_colormaps')
    groupV.add_argument('--colorbar',
                         metavar="STRING",
                         dest='sColorBar',
                         default='linear',
                         help='Colorbar: "linear" or "mirror": "linear" gives a gradient from lowest to highest value. "mirror" centers the colorbar around zero, with min/max=max(abs(max), abs(min)).  [default: "linear"]')
    groupV.add_argument('--colormax',
                         metavar="FLOAT",
                         type=float,
                         dest='fColorMax',
                         default=None,
                         help='Set maximum value for colorbar [default: automatically]')
    groupV.add_argument('--colormin',
                         metavar="FLOAT",
                         type=float,
                         dest='fColorMin',
                         default=None,
                         help='Set minimum value for colorbar (only effective if colobar=linear) [default: automatically]')
    groupV.add_argument('--bartitle',
                         metavar='STRING',
                         type=str,
                         dest='sLegendTitle',
                         default='',
                         help='Title of the colorbar.  [default: None]')
    groupV.add_argument('--no_colorbar',
                         dest='no_colorbar',
                         action='store_true',
                         default=False,
                         help='Hide colorbar. [default: False]')
    groupV.add_argument('--rfontsize',
                         metavar="INT",
                         type=int,
                         dest='iRFont',
                         default=None,
                         help='Set the font-size fo the row labels. [default: Set automatically]')
    groupV.add_argument('--cfontsize',
                         metavar="INT",
                         type=int,
                         dest='iCFont',
                         default=None,
                         help='Set the font-size fo the column labels. [default: Set automatically]')
    groupV.add_argument('--no_rlabels',
                         dest='bNo_row_labels',
                         action='store_true',
                         default=False,
                         help='Hide row labels. [default: False]')
    groupV.add_argument('--no_clabels',
                         dest='bNo_col_labels',
                         action='store_true',
                         default=False,
                         help='Hide column labels. [default: False]')
    groupV.add_argument('--clabels_degree',
                         metavar='INT',
                         dest='bCol_labels_degree',
                         type=int,
                         default=270,
                         help='(Experimental) Set the degree of the column labels, e.g. set to 0 degree for horizontal. [default: vertical/270]')
    groupV.add_argument('--gridAlpha',
                         metavar="FLOAT",
                         type=float,
                         dest='fGridAlpha',
                         default=0.,
                         help='Set transparency of the grid. 0<= Alpha <=1.  [default: noGrid/0.]')
    
    groupC = parser.add_argument_group('Clustering', ' Clustering specific arguments:')
    groupC.add_argument('--cmethod',
                         metavar="STRING",
                         dest='cMethod',
                         default=None,
                         help='Perform clustering of columns with linkage algorithm: average, single, centroid, complete, weighted, ward [default: None]')
    groupC.add_argument('--rmethod',
                         metavar="STRING",
                         dest='rMethod',
                         default=None,
                         help='Perform clustering of rows with linkage algorithm: average, single, centroid, complete, weighted, ward [default: None]')
    groupC.add_argument('--cmetric',
                         metavar="STRING",
                         dest='cMetric',
                         default='euclidean',
                         help='Metric for column distances: Select one from http://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.pdist.html [default: euclidean]')
    groupC.add_argument('--rmetric',
                         metavar="STRING",
                         dest='rMetric',
                         default='euclidean',
                         help='Metric for row distances: Select one from http://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.pdist.html [default: euclidean]')
    
    groupD = parser.add_argument_group('Dendrogram', 'Dendrogram specific arguments:')
    groupD.add_argument('--no_cdendrogram',
                         dest='bNo_col_dendro',
                         action='store_true',
                         default=False,
                         help='Hide column dendrogram. [default: False]')
    groupD.add_argument('--no_rdendrogram',
                         dest='bNo_row_dendro',
                         action='store_true',
                         default=False,
                         help='Hide row dendrogram. [default: False]')
    groupD.add_argument('--dcolor',
                         metavar="STRING",
                         dest='dcolor',
                         default='#515151',
                         help='HTML color-code for dendrogram color. [defaul: #515151]')

    
    args = parser.parse_args()
    return args, parser

def main():
    args, parser = parse_cmdline()

    import gzip, bz2, zipfile
    # open file
    if args.sFile in ['-', 'stdin']:
        oF = sys.stdin
    elif args.sFile.split('.')[-1] == 'gz':
        oF = gzip.open(args.sFile)
    elif args.sFile.split('.')[-1] == 'bz2':
        oF = bz2.BZFile(args.sFile)
    elif args.sFile.split('.')[-1] == 'zip':
        oF = zipfile.Zipfile(args.sFile)
    else:
        oF = open(args.sFile)

    if args.fGridAlpha < 0 or args.fGridAlpha > 1:
        parser.error('--gridAlpha need to be between 0 and 1. EXIT.')

    aMethods = ['average', 'single', 'centroid', 'complete', 'weighted', 'ward']
    aMetrics = ['euclidean', 'braycurtis', 'canberra', 'chebyshev', 'cityblock', 'correlation', 'cosine', 'dice', 'hamming','jaccard', 'kulsinski', 'mahalanobis', 'matching','minkowski', 'rogerstanimoto', 'russellrao', 'seuclidean','sokalmichener', 'sokalsneath', 'sqeuclidean', 'yule']
    aColors = plt.cm.datad.keys() + ['red_black_green', 'solarized']

    if args.cMethod:
        if args.cMethod not in aMethods:
            parser.error('Incorrect method for column clustering %s. EXIT\n' %(args.cMethod))
    column_method = args.cMethod

    if args.rMethod:
        if args.rMethod not in aMethods:
            parser.error('Incorrect method for row clustering %s. EXIT\n' %(args.rMethod))
    row_method = args.rMethod

    if args.sColor not in aColors:
        parser.error ('Incorrect color map. More info here: http://wiki.scipy.org/Cookbook/Matplotlib/Show_colormaps. EXIT.\n')
    color_gradient = args.sColor

    if args.cMetric not in aMetrics:
        parser.error('Incorrect metric for column clustering. Metric has to be one of %s .EXIT\n' %(','.join(aMetrics)))
    column_metric = args.cMetric

    if args.rMetric not in aMetrics:
        parser.error('Incorrect metric for row clustering. Metric has to be one of %s. EXIT\n' %(','.join(aMetrics)))
    row_metric = args.rMetric

    if args.sColorBar not in ["mirror", "linear"]:
        parser.error('Incorrect value for row colorbar. EXIT\n')

    # delimited file handler
    aMatrix, aRow, aCol = read_matrix(oF, args.sDEL, args.rHead, args.cHead)
    if aRow == []:
        no_row_labels = True
    else:
        no_row_labels = args.bNo_row_labels
    if aCol == []:
        no_col_labels = True
    else:
        no_col_labels = args.bNo_col_labels

    if args.bCol_labels_degree < 0 or args.bCol_labels_degree > 360:
        parser.error('Incorrect value for column_degree. has to be between 0 and 360. EXIT\n')

    if len(aMatrix) > 0:
        hmap(aMatrix,
             aRow,
             aCol,
             row_method,
             column_method,
             row_metric,
             column_metric,
             color_gradient,
             filename=args.sFileOut,
             colorbar=args.sColorBar,
             no_colorbar=args.no_colorbar,
             no_row_labels=no_row_labels,
             no_col_labels=no_col_labels,
             iRowFont=args.iRFont,
             iColFont=args.iCFont,
             fWidth=args.fWidth,
             fHeight=args.fHeight,
             colormax=args.fColorMax,
             colormin=args.fColorMin,
             col_labels_degree=args.bCol_labels_degree,
             bNoTight=args.bNoTight,
             sLegendTitle=args.sLegendTitle,
             no_row_dendrogram=args.bNo_row_dendro,
             no_col_dendrogram=args.bNo_col_dendro,
             dcolor=args.dcolor,
             gridAlpha = args.fGridAlpha)

    return

if __name__ == '__main__':
    sys.exit(main())
