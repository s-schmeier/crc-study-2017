#!/usr/bin/env python
"""

Read a set of gene_ids and calculate the enrichment in any msigdb category
using the fisher exact test.


VERSION HISTORY

0.4.0  2017/03/31    Consoidated all dependencies into this script.
0.3.5  2015/08/20    More options for the figure.
0.3.4  2014/10/23    Atatach the % of genes in tageret from all genes with category
0.3.3  2014/10/21    Improoved the plotting
0.3.2  2014/10/10    Other ids possible. Put taxonomy as an option.
0.3.1  2014/10/10    Background can be all genes that hava a annotation in MSIGDB, remove NCBI requirement, remove HTML
0.3.0  2014/09/01    Major rework, background are now all protein-coding genes
                     of the species by default.
0.2.1  2014/08/29    Added plotting.
0.2    2014/08/21    Added custom background support, added URL
0.1    2013/10/25    Initial version.

COPYRIGHT Sebastian Schmeier (s.schmeier@gmail.com)
"""
__version__='0.4.0'
__date__='2017/03/31'
__email__='s.schmeier@gmail.com'
__author__='Sebastian Schmeier'
import sys, os, argparse, csv, time, random, gzip, bz2, zipfile, sets
from rpy2.robjects.packages import importr
from rpy2.robjects.vectors import FloatVector, IntVector
from rpy2.robjects import r as r_object

_stats = importr('stats', on_conflict="warn")

def bh_r (aPV):
    """
    use fdr to adust p-Values
    """
    p_adjust = _stats.p_adjust(FloatVector(aPV), method = 'BH')
    return list(p_adjust)
            

def fisher_r(a, alternative='g'):
    """
    a = [iGwithGOset, iGset-iGwithGOset, iGwithGOuni, iGuni-iGwithGOuni]
    a = [CAT1_Hit, CAT1_NoHit, CAT2_hit, CAT2_NoHit]
    
    alternative: 'g', 'l', 't'
    """
    oRmatrix = r_object.matrix(IntVector(a), nrow=2)
    res = _stats.fisher_test(oRmatrix, alternative=alternative)
    fPV = float([list(f) for f in list(res)][0][0])
    return fPV


def uniq(aList, sort=False):
    """
    Make a list unique.
    @param aList: Python list
    """
    if sort:
        return sorted(list(set(aList)))
    else:
        return list(set(aList))


def parse_cmdline():
    
    # parse cmd-line -----------------------------------------------------------
    sDescription = 'Read delimited file with human entrez gene-ids (CAN BE USED WITH OTHER IDS IF --info IS NOT USED) in a column and MSigDB-like mapping file and calculate enrichment of genes in each category in mapping-file. The background of genes are all protein-coding genes of the species with a category associated unless otherwise specified. The significance column shows indicator for statistical significance on the adjusted p-Value. *** < 0.001, ** < 0.01, * < 0.05, - >= 0.05.\n\nMSigDB mapping files can be downloaded from https://www.broadinstitute.org/gsea/downloads.jsp#msigdb' 
    sVersion='version %s, date %s' %(__version__,__date__)
    sEpilog = 'Copyright %s (%s)' %(__author__, __email__)

    oParser = argparse.ArgumentParser(description=sDescription,
                                      version=sVersion,
                                      epilog=sEpilog)
    oParser.add_argument('sFile',
                         metavar='FILE',
                         help='Delimited file. [if set to "-" or "stdin" reads from standard in]')
    oParser.add_argument('sMap',
                         metavar='MSigDB-MAPPING-FILE',
                         help="Tab-delimited mapping file with columns category<tab>URL<tab>entrez<tab>entrez<tab>...")
    oParser.add_argument('-d', '--delimiter',
                         metavar='STRING',
                         dest='sDEL',
                         default='\t',
                         help='Delimiter used in file.  [default: "tab"]')
    oParser.add_argument('-f', '--field',
                         metavar='INT',
                         type=int,
                         dest='iFIELD',
                         default=1,
                         help='Field / Column in file with entrez gene ids.  [default: "1"]')
    oParser.add_argument('-o', '--out',
                         metavar='STRING',
                         dest='sOut',
                         default=None,
                         help='Out-file. [default: "stdout"]')
    oParser.add_argument('-p', '--pvalue',
                         metavar='FLOAT',
                         type=float,
                         dest='fPV',
                         default=1.,
                         help='Consider only pathways where the adjusted p-Value is smaller as -p. [default: 1]')
    oParser.add_argument('-m', '--minimum',
                         metavar='MIN',
                         type=int,
                         dest='iMin',
                         default=2,
                         help='Consider only categories that are supported by at least -m number of genes. [default: 2]')
    oParser.add_argument('--info',
                         metavar='PATH',
                         dest='sINFO',
                         default=None,
                         help='Path to gene_info.gz file from NCBI for BG. If set to "None" do not consider. [download at ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene_info.gz; default path: "None"]')
    oParser.add_argument('--tax',
                         metavar='TAXONOMY',
                         dest='iTax',
                         type=int,
                         default=None,
                         help="Taxonomy-id of species to consider. Needs to be specified if using --info.")
    oParser.add_argument('--bg',
                         metavar='FILE',
                         dest='sBG',
                         default=None,
                         help='Path to file with genes to use as background. [default: None (use all genes that have a category associated)]')
    oParser.add_argument('--ncRNA',
                         action='store_true',
                         dest='bNCRNA',
                         default=False,
                         help='Include ncRNA in the UNIVERSE. [default: False]')

    group1 = oParser.add_argument_group('Bargraph', 'Plotting arguments:')
    group1.add_argument('--bargraph',
                         metavar="FILENAME",
                         dest='sBar',
                         type=str,
                         default=None,
                         help='Make a bargraph.')
    group1.add_argument('--width',
                         metavar='WIDTH',
                         dest='fW',
                         type=float,
                         default=8.,
                         help="Width of figure in inches [Default: 8]")
    group1.add_argument('--pline',
                         metavar='FLOAT',
                         dest='fPL',
                         type=float,
                         default=0.05,
                         help="FDR postion for vertical red significance line. [Default: 0.05]")
    group1.add_argument('--fdrplot',
                         metavar='FLOAT',
                         dest='fFDRPLOT',
                         type=float,
                         default=1.,
                         help="Only plot categories <= this FDR. [Default: 1]")
    group1.add_argument('--top',
                         metavar="INT",
                         dest='iTop',
                         type=int,
                         default=None,
                         help='Only plot top x. [Default: All]')
    group1.add_argument('--pct',
                         dest='bPCT',
                         action="store_true",
                         default=False,
                         help='Sort bargraph based on percentage. [Default: based on pValue]')
    group1.add_argument('--nocounts',
                         dest='bNoCount',
                         action="store_true",
                         default=False,
                         help='Remove counts from graph. [Default: False]')
    group1.add_argument('--nofdr',
                         dest='bNoFDR',
                         action="store_true",
                         default=False,
                         help='Remove FDR-line graph. [Default: False]')

    oArgs = oParser.parse_args()
    return oArgs, oParser


def load_file(s):
    if s in ['-', 'stdin']:
        oF = sys.stdin
    elif s.split('.')[-1] == 'gz':
        oF = gzip.open(s)
    elif s.split('.')[-1] == 'bz2':
        oF = bz2.BZFile(s)
    elif s.split('.')[-1] == 'zip':
        oF = zipfile.Zipfile(s)
    else:
        oF = open(s)
    return oF


def make_bar(a, sFile, width, sig, bNoCounts, bNoFDR):
    """
    a: [(FDR, YTICKLABEL, #TARGETS), (),...]
    """
    try:
        import matplotlib.pyplot as plt
    except ImportError:
        sys.stderr.write('matplotlib module is needed for bargraph. Skip.\n')
        return
    try:
        import numpy as np
    except ImportError:
        sys.stderr.write('numpy module is needed for bargraph. Skip.\n')
        return
    try:
        import matplotlib.cm as cm
    except ImportError:
        sys.stderr.write('matplotlib cm is needed for bargraph. Skip.\n')
        return

    colors = cm.Blues(np.linspace(0.0, 0.9, len(a))) # 0.9 instead of 1 for
    
    fWidth = width
    fHeight = len(a)*0.3
    
    fig = plt.figure(figsize=(fWidth, fHeight))
    bgColor="#eaeaf2"
    ax = fig.add_subplot(1, 1, 1, axisbg=bgColor)

    ax.spines['right'].set_visible(False) # get rid of right axis
    ax.spines['top'].set_visible(False) # get rid of top axis
    ax.spines['left'].set_visible(False) # get rid of left axis
    ax.spines['bottom'].set_visible(False) # get rid of left axis
    ax.xaxis.set_ticks_position('none') # no tick positions on the x-axis
    ax.yaxis.set_ticks_position('none') # no tick positions on the x-axis
    #ax.yaxis.set_ticks_position('left')
    #ax.get_yaxis().set_tick_params(direction='out')
    
    aX = [-1*np.log(t[1]) for t in a]
    aX.reverse()
    aIndex = [t[0] for t in a]
    aIndex.reverse()
    aTargets = ['%i (%.2f)'%(t[2],t[4]) for t in a]
    #aTargets = [str(t[2]) for t in a]
    aTargets.reverse()

    yPos = range(1,len(aX)+1)
    rects = ax.barh(yPos, aX, edgecolor = "#D4DCE6", align='center', color=colors, height=0.8, alpha=0.70, zorder=3)
    #rects = ax.barh(yPos, aX, edgecolor="#000099", align='center', color='#0066CC', height=0.8, alpha=0.3)
    plt.ylim(0,len(aX)+1)
    plt.yticks(yPos, aIndex) # plot category names
    plt.xlabel('-log(FDR)') 
    plt.ylabel('Category')

    plt.grid(axis='x', alpha=1, color="#FFFFFF", lw=1, zorder=0, linestyle='-')
    
    # set fontsize
    plt.setp(ax.get_yticklabels(), fontsize=12)
    plt.setp(ax.get_xticklabels(), fontsize=12)
    plt.setp(ax.xaxis.label, fontsize=12)
    plt.setp(ax.yaxis.label, fontsize=12)
    plt.setp(ax.title, fontsize=12)

    if not bNoCounts:
        # attach number of target genes
        for ii,rect in enumerate(rects):
            width = rect.get_width()
            plt.text(width+0.02,
                     np.ceil(rect.get_y()),
                     '%s'%(aTargets[ii]),
                      ha='left',
                      va='center',
                      color="k")

    if not bNoFDR:
        ax.axvline(-1*np.log(sig),linewidth=3, color='r', alpha=0.65, zorder=3)        
        plt.text(-1*np.log(sig)-0.48,
                ax.get_ylim()[1]+0.3,
                "FDR=%s"%(sig),
                color="r",
                fontsize=12)

    ## yPos = range(1,len(aX)+1)
    ## ax2 = ax.twinx()
    ## ax2.set_yticks(yPos)
    ## ax2.set_yticklabels(aTargets)
    
    fig.savefig(sFile, bbox_inches='tight', dpi=75)


def main():
    oArgs, oParser = parse_cmdline()

    if oArgs.iMin < 1: oParser.error('-m has to be an integer > 0. EXIT.')
    else: iMin = oArgs.iMin - 1
        
    if oArgs.fPV <= 0 or oArgs.fPV > 1: oParser.error('-p has to be > 0 and < 1. EXIT.')
    else: fPVmax = oArgs.fPV

    if oArgs.iFIELD < 0: oParser.error('Field -f has to be an integer > 0. EXIT.\n')
    else: iF = oArgs.iFIELD - 1
        
    # open mapping file
    oFmap = load_file(oArgs.sMap)
 
    ## BACKGROUND
    dUNIVERSE = {}
    bMSIGBG = False
    if oArgs.sINFO:
        if not oArgs.iTax:
            oParser.error('--tax needs to be specified if using --info. EXIT.')

        aClasses = ["protein-coding"]
        if oArgs.bNCRNA: aClasses.append("ncRNA")
            
        try:
            from seb.Bio.NCBI import GENE_INFO
            oNCBI = GENE_INFO(oArgs.sINFO, taxonomy=oArgs.iTax, classes = aClasses)
        except ImportError:
            sys.stderr.error('Could not import module NCBI. Could not create background (all genes), EXIT.')

        dUNIVERSE = sets.Set(oNCBI.get_all_ids())
    elif oArgs.sBG:
        aBG = []
        oBG = load_file(oArgs.sBG)
        for a in csv.reader(oBG): aBG.append(a[0])
        dUNIVERSE = sets.Set(aBG)
    else:
        bMSIGBG = True ## USE MSIGDB-file genes + target genes as universe
        

    dGENE2CAT = {}
    dCAT = {}
    dCAT2URL = {}
    for a in csv.reader(oFmap, delimiter='\t'):
        dCAT2URL[a[0]] = a[1]
        for sGENEID in a[2:]: ## for all genes in category
            if bMSIGBG: ## UNIVERSE IS TAKEN FROM THE MSIGDB FILE (target genes are added later)
               dUNIVERSE[sGENEID] = None  
            else: ## Already have universe, check if the gene is in 
                if sGENEID not in dUNIVERSE:
                    continue ## don't do anything
               
            dCAT[a[0]] = dCAT.get(a[0], []) + [sGENEID]
            dGENE2CAT[sGENEID] = dGENE2CAT.get(sGENEID, []) + [a[0]]

    dUNIVERSE = sets.Set(dUNIVERSE)
    
    # open file, load target gene set
    oF = load_file(oArgs.sFile)
    dGENES = {}
    for a in csv.reader(oF, delimiter = oArgs.sDEL): dGENES[a[iF]] = None
    iInputGenes = len(dGENES)
    dGENES_temp = sets.Set(dGENES.keys())

    if oArgs.sBG or bMSIGBG:
        ## this has historical reasons, if you do not supply a bg,
        ## the universe contains also the target genes, thus add here target genes if bg supplied
        ## or BG is taken from MSigDB
        dUNIVERSE = dUNIVERSE | dGENES_temp  # add target genes to universe
        dGENES = dGENES_temp
        dGENES_NOT_IN_UNIVERSE = []
    else:
        ## FIND GENES IN SUBMITTED TARGET THAT ARE NOT IN THE UNIVERSE
        ## As UNIVERSE is taken from the entrez gene file, we delete all genes from target set
        ## that ae not in the all genes file.
        dGENES_NOT_IN_UNIVERSE = dGENES_temp - dUNIVERSE
        ## DELETE TARGET GENES NOT IN UNIVERSE
        dGENES = dGENES_temp - dGENES_NOT_IN_UNIVERSE


    iNumGenesUNI = len(dUNIVERSE)
        
    if not oArgs.sOut:
        oFout = sys.stdout
    elif oArgs.sOut in ['-', 'stdin']:
        oFout = sys.stdout
    elif oArgs.sOut.split('.')[-1] == 'gz':
        oFout = gzip.open(oArgs.sOut, 'wb')
    else:
        oFout = open(oArgs.sOut, 'w')
    
    t1 = time.time()
            
    # get all categories and number of genes in the set for the target genes
    dSET = {}
    dGset = {}
    for sGene in dGENES:
        try:
            aCAT = dGENE2CAT[sGene]
        except KeyError:
            aCAT = None
        if aCAT:
            dGset[sGene] = None # count uniq genes with categories associtaed
            for sC in aCAT:
                dSET[sC] = dSET.get(sC, []) + [sGene]

    iNumGenesSET = len(dGENES)
    
    for k,v in dSET.items():
        dSET[k] = uniq(v)

    sys.stderr.write('--- Gene Set Enrichment Analysis ---\n\n#unique genes/lines in input: %i\n#unique target genes found in the used gene universe: %i (part of the analysis)\nTARGET-GENES ommited: %s\n#unique genes in set with any category associated: %i\n#unique genes in universe: %i \n\n#categories associated to gene set: %i\n#categories associated to universe: %i\n\n' %(iInputGenes, len(dGENES), ','.join(dGENES_NOT_IN_UNIVERSE) ,len(dGset) ,iNumGenesUNI, len(dSET), len(dCAT)))

    aRESULT = []
    for sCat in dSET:
        iHITset = len(dSET[sCat])
        if iHITset  >= iMin:
            iHITuni   = len(dCAT[sCat])  
            iNOHITset = iNumGenesSET - iHITset
            iHITbg    = iHITuni - iHITset
            iNOHITbg  = iNumGenesUNI-iNumGenesSET-iHITbg
            
            ## contingency table
            a = [iHITset, iNOHITset, iHITbg, iNOHITbg]

            ## TEST
            ## FISHER EXACT TEST
            fPV = fisher_r(a)
            aRESULT.append((fPV,iHITset,iHITbg,sCat,iNOHITset,iNOHITbg))
        
    aRESULT.sort()
    aPV = [t[0] for t in aRESULT]
    aPVfdr = bh_r(aPV)
    
    if oArgs.sINFO:
        oFout.write('CATEGORY\tCATEGORY-URL\t#HIT_SET\t#HIT_BG\t#NOHIT_SET\t#NOHIT_BG\tPCT_HIT\tPVALUE\tFDRADJ\tSIG\tGENES-IN-SET-WITH-CAT\tGENE-SYMBOL\tGENE-NAME\n')
    else:
        oFout.write('CATEGORY\tCATEGORY-URL\t#HIT_SET\t#HIT_BG\t#NOHIT_SET\t#NOHIT_BG\tPCT_HIT\tPVALUE\tFDRADJ\tSIG\tGENES-IN-SET-WITH-CAT\n')
            
    i=0
    aBAR = []
    for t in aRESULT:
        if aPVfdr[i] > fPVmax: continue
        if aPVfdr[i] < 0.001:
            sStar = '***'
        elif aPVfdr[i] < 0.01:
            sStar = '**'
        elif aPVfdr[i] < 0.05:
            sStar = '*'
        else:
            sStar = '-'

        fPCT = (t[1]*100)/float(t[1]+t[2])
        if aPVfdr[i] <= oArgs.fFDRPLOT:
            aBAR.append((t[3],aPVfdr[i], t[1], t[2], fPCT))
                
        aOUT = [t[3],
                dCAT2URL[t[3]],
                t[1],
                t[2],
                t[4],
                t[5],
                '%.4f'%(fPCT),
                '%.5e'%t[0],
                '%.5e'%aPVfdr[i],
                sStar,
                ','.join(dSET[t[3]])]

        
        if oArgs.sINFO:
            aSYM = []
            aNAME = []
            for sID in [s.strip() for s in aOUT[-1].split(',')]:
                aSYM.append(oNCBI.get_symbol(sID))
                aNAME.append(oNCBI.get_name_full(sID))
            aOUT.append(','.join(aSYM))
            aOUT.append(','.join(aNAME))
            
            oFout.write('%s\t%s\t%i\t%i\t%i\t%i\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n' %(tuple(aOUT)))
        else:
            oFout.write('%s\t%s\t%i\t%i\t%i\t%i\t%s\t%s\t%s\t%s\t%s\n' %(tuple(aOUT)))
        i+=1

    if oArgs.sBar:
        if len(aBAR) == 0:
            sys.stderr.write('Attention: No figure created as there are no categories to plot.')
        else:
            if oArgs.bPCT:
                aBAR = [(t[4], t) for t in aBAR]
                aBAR.sort()
                aBAR.reverse()
                aBAR = [t[1] for t in aBAR]
            if oArgs.iTop:
                aBAR= aBAR[:oArgs.iTop]
            make_bar(aBAR, oArgs.sBar, oArgs.fW, oArgs.fPL, oArgs.bNoCount, oArgs.bNoFDR)
        
        
    sys.stderr.write("RUNTIME: %f sec\n"%(time.time() - t1))

    return
        
if __name__ == '__main__':
    sys.exit(main())

