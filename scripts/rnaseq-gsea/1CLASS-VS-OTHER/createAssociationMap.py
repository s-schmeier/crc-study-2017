#!/usr/bin/env python
import sys, copy, collections
from optparse import OptionParser # for good program option parsing
__version__= '0.2.3'
__date__= '2014/02/28'

def parse_arguments():
    # parse cmd-line -----------------------------------------------------------
    usage = "usage: %prog [<2/3-col-File>]\n\tSebastian Schmeier"
    description = 'From a file with 2 or 3 col, build a heat map association. Col 2 will be columns, Col 1 will be rows. If Col 3 : association value. e.g.expression data.'

    version='version %s, date %s' %(__version__,__date__)

    oParser = OptionParser(usage=usage, version=version, description=description)

    oParser.add_option("-a", "--header",
                      action="store_true",
                      default=False,
                      dest="bHeader",
                      help="Header exists in files (exclude)")
    oParser.add_option("-d", "--default",
                      metavar="DefaultValue",
                      default="0",
                      dest="iDefault",
                      help="Default value for non associations [default = 0]")
    oParser.add_option("-v", "--value",
                      metavar="AssociationValue",
                      default="1",
                      dest="iAssociatedValue",
                      help="Default value for associations [default = 1]")
    oParser.add_option("-c", "--column",
                      metavar="AssociationValueColumn",
                      default="0",
                      dest="iAssociatedValueCol",
                      help="Default value for associations [default = 0]")
    oParser.add_option("-f", "--sep",
                      metavar='FIELDSEPERATOR',
                      dest="sSep",
                      default='\t',
                      help="fieldseperator for columns of files. (default=\\t)")
    oParser.add_option("--sortcol",
                      action="store_true",
                      default=False,
                      dest="bSORTc",
                      help="Sort Heatmap cols.")
    oParser.add_option("--sortrow",
                      action="store_true",
                      default=False,
                      dest="bSORTr",
                      help="Sort Heatmap rows.")


    (dOptions, aArgs) = oParser.parse_args()
    return dOptions, aArgs, oParser


if __name__ == '__main__':


    dOptions, aArgs, oParser = parse_arguments()
    # capture errors -------------------------------------------------------
    if len(aArgs)==0:
        try:
            sFile = sys.stdin
        except:
            oParser.error('Exit. No input.')
    else:
        try:
            sFile = open(aArgs[0])
        except IOError:
            oParser.error('Exit. Can not open %s.'%sFile)

    sSep = dOptions.sSep
    iAssociatedValue = dOptions.iAssociatedValue
    iDefault = dOptions.iDefault
    bHeader = dOptions.bHeader
 
    # START
    aLines = sFile.readlines()
    aLines = [s.strip() for s in aLines]

    if bHeader:
        aHeader = aLines.pop(0).split(sSep)
        sCol = aHeader[0]+'/'+aHeader[1]
    else:
        sCol = 'Column'

    # find uniq elements
    dCol1 = collections.OrderedDict()
    dCol2 = collections.OrderedDict()
    for s in aLines:
        aSplit = s.split(sSep)
        dCol1[aSplit[0].strip()] = None
        dCol2[aSplit[1].strip()] = iDefault


    for k in dCol1: dCol1[k] = copy.copy(dCol2)

    # fill the matrix
    for s in aLines:
        aSplit = s.split(sSep)
        if len(aSplit) == 3:
            fValue = float(aSplit[2].strip())
        else:
            # association map
            fValue = iAssociatedValue

        sROW = aSplit[0].strip()
        sSample = aSplit[1].strip()

        dCol1[sROW][sSample] = fValue


    aSamples = dCol2.keys()
    aCol = dCol1.keys()
    if dOptions.bSORTc:
        aSamples.sort()
       
    if dOptions.bSORTr:
        aCol.sort()
    
    # print Header
    print '%s%s%s' %(sCol, sSep, sSep.join(aSamples))
    
    for k in aCol:
        a = []
        for s in aSamples:
            a.append(str(dCol1[k][s]))
        
        print '%s%s%s' %(k, sSep, sSep.join(a))
    
    
        
    
    
        
        
    
    
    
    
    

    
    

        
