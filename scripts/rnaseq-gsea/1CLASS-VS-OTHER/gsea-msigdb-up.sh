gmt=../../data/msigdb/crc.gmt
fdr="$1"
mkdir -p ${fdr}
for i in *.gz; do
    file=${fdr}/$(basename ${i} | sed 's/.txt.gz/.up.gsea.txt.gz/g')
    echo $file;
    zcat ${i} | awk -v fdr="$fdr" -F '\t' '{if ($5<fdr && $2>0) print $1}' | python msigdb.py - ${gmt} | gzip > ${file}
done
