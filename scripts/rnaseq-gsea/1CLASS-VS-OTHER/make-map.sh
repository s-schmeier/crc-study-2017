fdr="$1"
type="$2"
dir="$3"
outfilepdf="$4"

{ for i in ${dir}/*.${type}.gsea.txt.gz; do
    name=$(basename ${i} | sed 's/.gsea.txt.gz//g');
    #echo $name;
    zcat ${i} | tail -n +2 | awk -v FDR="$fdr" -v NAME="$name" -F '\t' '{OFS="\t"; if ($9<FDR) print $1,NAME,$9;}'; 
done } | \
python createAssociationMap.py --sortcol --sortrow -d 1 | \
python plot-hmap.py --colormax ${fdr} --clabels --rlabels --color Reds_r --rmethod complete - ${outfilepdf}