# GSEA run
For all CRC samples.
Based on raw counts derived through mapping with STAR and counting with HTSEQ-COUNT
We are running 1 class VS all OTHER using edgeR to id DEG (up/dn).
We use categories from MSigDB to calc. enrich. categories for up and dn genes.
Result are enriched categories for the CMS classes NOT individual samples.

```bash
# Install miniconda
curl -O https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
bash Miniconda2-latest-Linux-x86_64.sh

# Add conda dir to PATH
echo 'export PATH="~/miniconda2/bin:$PATH"' >> ~/.bashrc

# Install bioconda
conda config --add channels conda-forge
conda config --add channels defaults
conda config --add channels r
conda config --add channels bioconda

# make env
conda create --name crcgsea --file conda-packages.txt
source activate crcgsea

# Run workflow
make run
```