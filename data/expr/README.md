
The folder `raw_counts` contains compressed files of raw read counts for each sample.
The files contain Ensembl gene ID in the first column and the corresponding read count
in the second column.  

The file `counts_summary.havana.cols.uniqgenes.gz` contains summarised information from the files
in the `raw_counts`folder. In this file, the gene IDs were changed from Ensembl to Entrez gene IDs 
and genes are shown in rows while sample IDs are shown in columns. A number on the position
in the `x`-th row and `y`-th column is the read count for the `x`-th gene of the `y`-th sample.


The file `tpm.readyForClassifier.tsv` contains data from `counts_summary.havana.cols.uniqgenes.gz`
transformed to a form suitable as an input for the CMS classifier. 
 In this file, the Entrez gene IDs are in columns, sample IDs are in rows and a number on the 
position in the `x`-th row and `y`-th column is TPM value (transcript-per-million) of the gene 
`y` in the sample `x`. For the details of the transformation see 
`scripts/rnaseq-subtype-classification/summarize_and_transform_counts.R`.
The gene expression profiles from `tpm.readyForClassifier.tsv`
were used as input for `scripts/rnaseq-subtype-classification/classify_samples_with_CMSclassifier.R` 
to classify CRC samples into CMSs. 

Classification of the samples into CMS subtypes is available as the file 
`CMSclassifiedCRC.tpm.havana.tsv`.
