Outputs of the 16S rRNA metabarcoding analysis

Here we keep `otu_table.biom` file containing OTUs identified 
with QIIME's `pick_de_novo_otus.py` workflow. 

Files `otu_table_sorted_L*.txt.gz` are compressed outputs of 
QIIME's `summarize_taxa_through_plots.py` workflow for taxonomic
levels L2 and L6.
