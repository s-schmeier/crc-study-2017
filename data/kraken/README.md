# File description

## File used for differential taxa expression

- rnaseq-kraken-reports-combined-species-nocellines.txt.gz 

Counts of reads per taxa per sample.


## Files for comparing RNA-seq and 16S methodologies

- rnaseq-kraken-reports-combined-genus-abundances-nocellines.txt.gz
- rnaseq-kraken-reports-combined-phylum-abundances-nocellines.txt.gz

Files containing abundances of taxa in tems of percentage on all taxa within the sample.

## Files containing information on used bacterial taxa in the Kraken database

- Supplementary_table_K1.xlsx
- Supplementary_table_K2.xlsx

All NCBI refseq bacterial genomes with "Complete Genomes"- or "Chromosome"-level genomes were downloaded from NCBI FTP 
site (ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/) based on information in the ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt 
file as of 19th January 2017. 
A list of the genomes can be accessed in Supplementary_table_K1.xlsx. 
Additional genomes known to play role in CRC were added disregarding their genome status (see Supplementary_table_K2.xlsx). 
Using the genome fasta-files, a new Kraken database was created using "kraken-build --build" with default parameter.
The resulting database had a size of 131GB.
